/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');

var Vehicle = require('./../models/vehicle');
var Vehicles = Bookshelf.Collection.extend({model: Vehicle});
var Finance = require('./../models/finances');
var VehicleCost = require('./../models/vehiclecost');


var vehicle = {
        /***
         * Gibt alle Vehicles zurück
         * @param successCb
         * @param errorCb
         */
        getAll: function (req, res) {
            Vehicles.forge().fetch()
                .then(function (vehicles) {
                    Basic.respondToClient(res, 200, null, null, null, (vehicles));
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Fahrzeuge laden", err, null);
                });
        },

        getOne: function (req, res) {
            var id = req.params.id;

            if (id) {
                /**
                 * Get Employer Data without password & salt
                 */
                Vehicle.forge({'vehicle_id': id})
                    .fetch()
                    .then(function (vehicle) {
                        Basic.respondToClient(res, 200, null, null, null, vehicle);
                    })
                    .catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Fahrzeuge", err, null);
                    });
            } else {
                Basic.respondToClient(res, 500, "Fehler", "Kann Fahrzeug nicht laden. Keine ID erkannt", null, null);
            }
        },

        create: function (req, res) {
            var vehicle = req.body.vehicle;
            var vehicleCost = req.body.vehicle.purchase_price;

            if (vehicle && vehicle.mark && vehicle.purchase_price && vehicle.loading_volume) {
                /**
                 * Validate Customer
                 */
                Vehicle.forge().save({
                    mark: vehicle.mark,
                    purchase_price: vehicle.purchase_price,
                    loading_volume: vehicle.loading_volume,
                    brand: vehicle.brand,
                    model: vehicle.model,
                    consumption: vehicle.consumption
                }).then(function (vehicle) {
                    var vehicleID = vehicle.get('vehicle_id');

                    //Insert finances
                    Finance.forge({date: new Date().toLocaleDateString()}).fetch()
                        .then(function (finance) {
                            var vehicle_cost = finance.get('vehicle_cost');
                            vehicle_cost += vehicleCost;
                            finance.set('vehicle_cost', vehicle_cost);
                            finance.save().then(function () {

                                VehicleCost.forge().save({
                                    vehicle_id: vehicleID,
                                    sum: vehicleCost,
                                    date: new Date()
                                }).then(function () {
                                    Basic.respondToClient(res, 200, "Erfolg", "Finanzen und Fahrzeugkosten aktualisiert", null, null);
                                }).catch(function (err) {
                                    Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeugkosten nicht speichern!", err, null);
                                });

                            }).catch(function (err) {
                                Basic.respondToClient(res, 500, "Fehler", "Konnte Finanzen nicht aktualisieren!", null, null);
                            });
                        }).catch(function (err) {
                            Finance.forge().save({
                                date: new Date().toLocaleDateString(),
                                vehicle_cost: vehicleCost
                            }).then(function () {
                                VehicleCost.forge().save({
                                    vehicle_id: vehicleID,
                                    sum: vehicleCost,
                                    date: new Date()
                                }).then(function () {
                                    Basic.respondToClient(res, 200, "Erfolg", "Finanzen und Fahrzeugkosten aktualisiert", null, null);
                                }).catch(function (err) {
                                    Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeugkosten nicht speichern", err, null);
                                });
                            }).catch(function (err) {
                                Basic.respondToClient(res, 500, "Fehler", "Konnte Finanzen nicht eintragen", err, null);
                            });
                        });

                    //Basic.respondToClient(res, 200, "Erfolg", "Fahrzeug erfolgreich angelegt.", null, null);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeug nicht anlegen.", err, null);
                });
            } else {
                Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte Kennzeichen, Kaufpreis und Ladevolumen angeben", {error: ""}, null);
            }
        },

        update: function (req, res) {
            var id = req.params.id;
            var vehicle = req.body.vehicle;

            if (vehicle && vehicle.mark && vehicle.purchase_price && vehicle.loading_volume) {
                if (id) {

                    new Vehicle({vehicle_id: id})
                        .save({
                            mark: vehicle.mark,
                            purchase_price: vehicle.purchase_price,
                            loading_volume: vehicle.loading_volume,
                            status: vehicle.status,
                            brand: vehicle.brand,
                            model: vehicle.model,
                            consumption: vehicle.consumption
                        }).then(function () {
                            Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                        }).catch(function (err) {
                            Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                        });

                } else {
                    Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Fahrzeug nicht speichern", null, null);
                }
            } else {
                Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgefuellt", null, null);
            }

        }
        ,

        delete: function (req, res) {
            var id = req.params.id;
            if (id) {
                Vehicle.forge({vehicle_id: id}).destroy().then(function () {
                    Basic.respondToClient(res, 200, "Erfolg", "Fahrzeug erfolgreich entfernt", null, null);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeug nicht loeschen", err, null);
                });
            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Fahrzeug nicht loeschen", null, null);
            }
        }
        ,

        getVehicleIds: function (req, res) {
            Vehicles.forge().fetch({
                columns: ['vehicle_id']
            }).then(function (vehicleIDs) {
                Basic.respondToClient(res, 200, "Erfolg", "FahrzeugNummer erfolgreich geladen", null, vehicleIDs);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte FahrzeugNummern nicht laden", err, null);
            });
        }

    }
    ;

module.exports = vehicle;
