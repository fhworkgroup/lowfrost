/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Basic = require('./basic');

var TimeEstimation = require('./../models/time_estimation');
var TimeEstimations = Bookshelf.Collection.extend({model: TimeEstimation});
var Employer = require('./../models/employer');
var Finance = require('./../models/finances');
var EmployerCost = require('./../models/employercost');


var timeEstimation = {
    /***
     * Gibt alle Vehicles zurück
     * @param successCb
     * @param errorCb
     */
    getTimeEstimationByEmployer: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Tour Data
             */
                Bookshelf.knex.raw('SELECT * FROM time_estimation WHERE employer_id ='+id)
                .then(function (timeEstimation) {
                    Basic.respondToClient(res, 200, null, null, null, timeEstimation[0]);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Stunden", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Stunden nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        var hours = req.body.time_estimation;
        var employerID = req.body.time_estimation.employer_id;
        var totalTime = req.body.time_estimation.total;
        var date = Basic.formatDateOnly(req.body.time_estimation.date);

        if (hours && hours.date && hours.from_hour && hours.to_hour) {
             //save time Estimation
            TimeEstimation.forge().save({
                employer_id: hours.employer_id,
                date: date,
                from_hour: hours.from_hour,
                to_hour: hours.to_hour,
                total: hours.total
            }).then(function (timEst) {

                //get stundenlohn from employer
                Employer.forge({employer_id: employerID}).fetch({columns: ['hourly_wage']})
                    .then(function (hourlyWage) {
                        var employerCost = (hourlyWage.get('hourly_wage') * totalTime);
                        console.log("TEEEEST", hourlyWage.get('hourly_wage') * totalTime, hourlyWage.get('hourly_wage'), totalTime );

                        //Save employer Cost to finance
                        //Insert finances
                        Finance.forge({date: new Date().toLocaleDateString()}).fetch()
                            .then(function (finance) {
                                var employer_cost = finance.get('vehicle_cost');
                                employer_cost += employerCost;
                                finance.set('employer_cost', employer_cost);
                                finance.save().then(function () {

                                    EmployerCost.forge().save({
                                        employer_id: employerID,
                                        sum: employerCost,
                                        date: new Date()
                                    }).then(function () {
                                        Basic.respondToClient(res, 200, "Erfolg", "Stunden und Mitarbeiterkosten aktualisiert", null, null);
                                    }).catch(function (err) {
                                        Basic.respondToClient(res, 500, "Fehler", "Konnte Mitarbeiterkosten nicht speichern!", err, null);
                                    });

                                }).catch(function (err) {
                                    Basic.respondToClient(res, 500, "Fehler", "Konnte Finanzen nicht aktualisieren!", null, null);
                                });
                            }).catch(function (err) {
                                Finance.forge().save({
                                    date: new Date().toLocaleDateString(),
                                    employer_cost: employerCost
                                }).then(function () {
                                    EmployerCost.forge().save({
                                        employer_id: employerID,
                                        sum: employerCost,
                                        date: new Date()
                                    }).then(function () {
                                        Basic.respondToClient(res, 200, "Erfolg", "Stunden und Mitarbeiterkosten aktualisiert", null, null);
                                    }).catch(function (err) {
                                        Basic.respondToClient(res, 500, "Fehler", "Konnte Mitarbeiterkosten nicht speichern", err, null);
                                    });
                                }).catch(function (err) {
                                    Basic.respondToClient(res, 500, "Fehler", "Konnte Finanzen nicht eintragen", err, null);
                                });
                            });

                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Stundenlohn nicht abrufen", err, null);
                    });

                //Basic.respondToClient(res, 200, "Erfolg", "Stunden erfolgreich eingetragen.", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Stunden nicht anlegen.", err, null);
            });
            //}, function (err) {
            //    Basic.respondToClient(res, 201, "Stunden vorhanden", "Du hast die Stunden für dieses Datum bereits eingetragen", err, null);
            //});

        } else {
            Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte Felder ausfuellen", {error: ""}, null);
        }
    },

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            TimeEstimation.forge({time_est_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Stunden erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Stunden nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Stunden nicht loeschen", null, null);
        }
    }


};

function hasDate(id, date, resolve, reject) {

    new TimeEstimation().query(function (qb) {
        qb.where('employer_id', id);
        qb.where('date', date).then(function (time) {
            try {
                var d = time.get('date');
                resolve(d);
            } catch (e) {
                reject(e);
            }
        }).catch(function (err) {
            console.log(err, "ERROR REJECT");
            reject(err);
        })
    });

}

module.exports = timeEstimation;
