/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');
var Promise = require('bluebird');

var Employer = require('./../models/employer');
var Employers = Bookshelf.Collection.extend({model: Employer});
var Customer = require('./../models/customer');

var Delivers = require('./../models/delivers');
var Order = require('./../models/orders');
var OrderItem = require('./../models/orderItems');
var Product = require('./../models/product');
var Vehicle = require('./../models/vehicle');


var employer = {
    /***
     * Gibt alle Mitarbeiter zur�ck
     * @param successCb
     * @param errorCb
     */
    getAll: function (req, res) {
        Employers.forge().fetch()
            .then(function (employers) {
                Basic.respondToClient(res, 200, null, null, null, (employers));
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Kunden laden", err, null);
            });
    },

    getOne: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Employer Data without password & salt
             */
            Employer.forge({'employer_id': id})
                .fetch({
                    columns: [
                        'employer_id',
                        'email',
                        'username',
                        'surname',
                        'name',
                        'tel_nr',
                        'city',
                        'postcode',
                        'street',
                        'street_nr',
                        'country',
                        'role',
                        'hourly_wage',
                        'target_hours'
                    ]
                })
                .then(function (employer) {
                    Basic.respondToClient(res, 200, null, null, null, employer);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Kundendaten", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Profildaten nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        var userData = req.body.userData;

        if (userData && userData.username && userData.email && userData.password) {
            var password = userData.password;
            /**
             * Validate Customer
             */
            Auth.validateUser("employer", userData.email, function () {
                //if user found by email break up
                Basic.respondToClient(res, 500, "Email " + userData.email + " bereits vorhanden. Bitte waehlen Sie eine andere E-Mailadresse");
            }, function () {
                //hash pw with salt
                Auth.hash_pw(password, function (newHash) {
                    Employer.forge().save({
                        username: userData.username,
                        email: userData.email.toLowerCase(),
                        password: newHash.hash,
                        salt: newHash.salt,
                        role: userData.role,
                        hourly_wage: userData.hourly_wage,
                        target_hours: userData.target_hours,
                        surname: userData.surname,
                        name: userData.name,
                        country: userData.country,
                        city: userData.city,
                        postcode: userData.postcode,
                        street: userData.street,
                        street_nr: userData.street_nr,
                        tel_nr: userData.tel_nr
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Mitarbeiter erfolgreich angelegt.", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Mitarbeiter nicht anlegen.", err, null);
                    });

                }, function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte passwort nicht generieren", err, null);
                });
            });

        } else {
            Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte geben Sie zumindest Benutzername, E-Mail und Passwort an.", {error: ""}, null);
        }

    },

    update: function (req, res) {
        var id = req.params.id;
        var userData = req.body.userData;

        if (userData.surname && userData.name && userData.country && userData.city && userData.postcode && userData.street && userData.street_nr) {
            if (id) {

                new Employer({employer_id: id})
                    .save({
                        email: userData.email,
                        role: userData.role,
                        username: userData.username,
                        hourly_wage: userData.hourly_wage,
                        target_hours: userData.target_hours,
                        surname: userData.surname,
                        name: userData.name,
                        country: userData.country,
                        city: userData.city,
                        postcode: userData.postcode,
                        street: userData.street,
                        street_nr: userData.street_nr,
                        tel_nr: userData.tel_nr
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                    });

            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Nutzer nicht speichern", null, null);
            }
        } else {
            Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgef�llt", null, null);
        }

    },

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            Employer.forge({employer_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Mitarbeiter erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Mitarbeiter nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Mitarbeiter nicht loeschen", null, null);
        }
    },

    getDrivers: function (req, res) {
        Employer.forge().where('role', "employer").fetchAll()
            .then(function (drivers) {
                Basic.respondToClient(res, 200, "Erfolg", "Alle fahrer geladen", null, drivers);
            }).catch(function (err) {
                Basic.respondToClient(res, 200, "Fehler", "Konnte Fahrer nicht laden", err, null);
            });
    },

    getAssignments: function (req, res) {
        var id = req.params.id;
        var promises = [];
        var outerPromises = [];
        var responseData = [];
        var errors = [];

        if (id) {

            Delivers.forge().where('employer_id', id).fetchAll()
                .then(function (delivers) {
                    //iterate over orders
                    delivers.forEach(function (order) {
                        outerPromises.push(collectDeliveryData(order));
                    });

                    Promise.all(outerPromises).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Alle Daten geladen", errors, responseData);
                        console.log("FINISHED ALL");
                    });

                }).catch(function (err) {
                    errors.push("Fehler", "Konnte Lieferdaten nicht laden", errors, null);
                });

        } else {
            Basic.respondToClient(res, 404, "Keine ID", "Bitte geben Sie eine MITARBEITER ID als paramter an", null, null);
        }

        /***
         * Collects Vehicle Data and Productdata by order ID
         * @returns {bluebird|exports|module.exports}
         */
        function collectDeliveryData(order) {
            return new Promise(function (resolve, reject) {
                var delivery_id = order.get('delivery_id');
                var order_id = order.get('order_id');
                var vehicle_id = order.get('vehicle_id');

                /**
                 * Represents one assignment for an employer
                 * @type {{delivery_id: *, order_id: *, vehicle_id: *, vehicle: {}, products: Array}}
                 */
                var assignment = {
                    delivery_id: delivery_id,
                    order_id: order_id,
                    vehicle: {},
                    products: [],
                    total: 0
                };

                Order.forge({order_id: order_id}).fetch().then(function (order) {
                    if(order.get('status') !== 'fertig') {
                        //IF ORDER IS NOT ALREADY FINISHED CONTINUE
                        Vehicle.forge({vehicle_id: vehicle_id}).fetch()
                            .then(function (vehicle) {
                                assignment.vehicle = vehicle.attributes;

                                OrderItem.forge().where('order_id', order_id).fetchAll({
                                    columns: ['product_id', 'amount']
                                }).then(function (prods) {
                                    prods.forEach(function (item) {
                                        //product
                                        var amount = item.get('amount');

                                        promises.push(Product.forge({product_id: item.get('product_id')}).fetch()
                                            .then(function (product) {
                                                product.set({amount: amount});
                                                assignment.products.push(product);
                                                assignment.total += product.get('retail_price') * amount;
                                            }).catch(function (err) {
                                                errors.push(err);
                                            }));
                                    });

                                    Promise.all(promises).then(function() {
                                        responseData.push(assignment);
                                        resolve();
                                    });

                                }).catch(function (err) {
                                    errors.push(err);
                                    reject();
                                });

                            }).catch(function (err) {
                                errors.push(err);
                                reject();
                            });

                    } else {
                        resolve();
                    }
                }).catch(function (err) {
                    errors.push(err);
                    reject();
                });
            })
        }

    }

};

module.exports = employer;
