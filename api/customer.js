/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */


/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');

var Customer = require('./../models/customer');
var Customers = Bookshelf.Collection.extend({model: Customer});
var Basket = require('./../models/delivers');


/**
 * customer object that contains all customer related DB calls
 *
 * @type {{getAll: Function, getOne: Function, create: Function, update: Function, delete: Function, saveProfile: Function, addToBasket: Function}}
 */
var customer = {
    /***
     * Gibt alle Kunden zur�ck
     * @param successCb
     * @param errorCb
     */
    getAll: function (req, res) {
        Customers.forge().fetch()
            .then(function (customers) {
                Basic.respondToClient(res, 200, null, null, null, (customers));
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Kunden laden", err, null);
            });

    },

    getOne: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Customer Data without password & salt
             */
            Customer.forge({'customer_id': id})
                .fetch({
                    columns: [
                        'customer_id',
                        'email',
                        'username',
                        'surname',
                        'name',
                        'tel_nr',
                        'city',
                        'postcode',
                        'street',
                        'street_nr',
                        'country',
                        'role'
                    ]
                })
                .then(function (customer) {
                    Basic.respondToClient(res, 200, null, null, null, customer);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Kundendaten", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Profildaten nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        var userData = req.body.userData;

        if (userData && userData.username && userData.email && userData.password) {
            var password = userData.password;
            /**
             * Validate Customer
             */
            Auth.validateUser("customer", userData.email, function () {
                //if user found by email break up
                Basic.respondToClient(res, 500, "Email " + userData.email + " bereits vorhanden. Bitte w�hlen Sie eine andere E-Mailadresse");
            }, function () {
                //hash pw with salt
                Auth.hash_pw(password, function (newHash) {
                    Customer.forge().save({
                        username: userData.username,
                        email: userData.email.toLowerCase(),
                        password: newHash.hash,
                        salt: newHash.salt,
                        surname: userData.surname,
                        name: userData.name,
                        country: userData.country,
                        city: userData.city,
                        postcode: userData.postcode,
                        street: userData.street,
                        street_nr: userData.street_nr,
                        tel_nr: userData.tel_nr
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Kunde erfolgreich angelegt.", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Kunde nicht anlegen.", err, null);
                    });

                }, function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte passwort nicht generieren", err, null);
                })

            });

        } else {
            Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte geben Sie zumindest Benutzername, E-Mail und Passwort an.");
        }

    },

    update: function (req, res) {
        var id = req.params.id;
        var userData = req.body.userData;

        if (userData.surname && userData.name && userData.country && userData.city && userData.postcode && userData.street && userData.street_nr) {
            if (id) {
                console.log(id);

                new Customer({customer_id: id})
                    .save({
                        email: userData.email,
                        username: userData.username,
                        surname: userData.surname,
                        name: userData.name,
                        country: userData.country,
                        city: userData.city,
                        postcode: userData.postcode,
                        street: userData.street,
                        street_nr: userData.street_nr,
                        tel_nr: userData.tel_nr
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                    });

            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Nutzer nicht speichern", null, null);
            }
        } else {
            Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgef�llt", null, null);
        }

    },

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            Customer.forge({customer_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Kunde erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Kunde nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte NUtzer nicht loeschen", null, null);
        }
    }

};


module.exports = customer;
