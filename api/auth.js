/***
 * This module handles login & registering a user.
 * It uses bookshelf to handle DB calls
 *
 * @type {*|exports|module.exports}
 */
var express = require('express');
//custom node module
var basic = require('./basic');

var Customer = require('./../models/customer.js');
var Basket = require('./../models/delivers');

var Employer = require('./../models/employer.js');

var jwt = require('jwt-simple');
var bcrypt = require('bcryptjs');
var knex = require('knex');


var auth = {
    login: function (req, res) {
        var role = (req.body && req.body.x_role) || (req.query && req.query.x_role) || req.headers['x-role'];
        var email = req.body.email.toLowerCase() || '';
        var password = req.body.password || '';

        if (email === "" && password === "") {
            basic.respondToClient(res, 401, "Bitte fuellen Sie alle Felder aus!");
            return;
        }

        auth.validate(role, email, password, function (user) {
            //if auth is success, generate a token
            //and send it to the client
            res.json(genToken(user));
        }, function (status, err) {
            basic.respondToClient(res, status, "Ups", "Bitte prüfe dein Passwort und Oder Email", err, null);
        }, res);

    },
    /**
     * Register function for customers
     *
     * @param req
     * @param res
     */
    register: function (req, res) {
        var username = req.body.username || '';
        var email = req.body.email.toLowerCase() || '';
        var password = req.body.password || '';

        /**
         * Error handling if credentials are empty
         */
        if (email === "" || password === "" || username === "") {
            basic.respondToClient(res, 401, "Bitte fuellen Sie alle Felder aus!");
            return;
        }

        /**
         * Here we check if the email adress is already in use
         *
         * returns error if email is already in use
         */
        auth.validateUser("customer", email, function (response) {
            //IF USER EXIST SEND MESSAGE
            basic.respondToClient(res, 403, "E-Mail vergeben", "E-Mailadresse " + response.email + " bereits vergeben", null, null);
        }, function (status, data) {
            //IF USER DOESEN�T EXIST GENERATE HASH & SALT AND SAVE CREDENTIALS TO DB

            //generate new hash & salt
            auth.hash_pw(password, function (newHash) {
                //store hash & salt in customer table
                Customer.forge({
                    username: username,
                    salt: newHash.salt,
                    password: newHash.hash,
                    email: email
                }).save().then(function () {
                    basic.respondToClient(res, 200, "Willkommen", "Erfolgreich registriert", null, null);
                }).catch(function (err) {
                    basic.respondToClient(res, 500, "Speichern Fehlgeschlagen", "Konnte Nutzer:" + email + " nicht  speichern", err, null);
                });
            }, function (err) {
                basic.respondToClient(res, 500, null, "Speichern fehlgeschlagen beim Benutzer " + email, err, null);
            });

        });

    },
    /***
     * Here we check the email & password and run an query against our database.
     * If email or password is wrong it returns errorMessage as Callback
     * @param email - user email string
     * @param req_hash - user password string (is a hash)
     * @param successCb - callback function requires userModel object
     * @param errorCb - errorCb function requires status number, data object
     */
    validate: function (role, email, req_hash, successCb, errorCb, res) {

        new Customer({email: email})
            .fetch()
            .then(function (customer) {
                //get hash stored in database from user model
                var db_hash = customer.get('password');
                compare_pw(req_hash, db_hash, function () {
                    successCb({
                        id: customer.get('customer_id'),
                        username: customer.get('username'),
                        role: customer.get('role'),
                        email: email
                    });
                }, function (err) {
                    basic.respondToClient(res, 401, "Fehler", "Benutzername oder Passwort falsch", err, null);
                });
            })
            .catch(function (err) {
                /***
                 * LOOK IN THE EMPLOYERS TABLE
                 */
                new Employer({email: email})
                    .fetch()
                    .then(function (employer) {
                        //get hash stored in database from user model
                        var db_hash = employer.get('password');
                        compare_pw(req_hash, db_hash, function () {
                            successCb({
                                id: employer.get('employer_id'),
                                username: employer.get('username'),
                                role: employer.get('role'),
                                email: email
                            });
                        }, function (err) {
                            basic.respondToClient(res, 401, "Fehler", "Benutzername oder Passwort falsch", err, null);
                        });
                    })
                    .catch(function (err) {
                        errorCb(err);
                    });
            });

    },

    //TODO:Refactor this if role is customer look in customer table if role is employer look in employer table a.s.o
    /***
     * Here we check if user exists by passing an email as argument.
     * Function responds with user object or false
     * @param email - user email string
     * @param successCb - function returns user object
     * @param errorCb - function returns false
     */
    validateUser: function (role, email, successCb, errorCb) {

        if (role === "customer") {
            /**
             * Check Customers First
             */
            new Customer({email: email})
                .fetch()
                .then(function (user) {
                    successCb({
                        id: user.get('customer_id'),
                        role: user.get('role'),
                        email: email
                    });
                })
                .catch(function (err) {
                    errorCb(err);
                });
        } else if (role === "employer" || role === "admin" || role === "accounting" || role === "owner") {
            /**
             * Check Customers First
             */
            new Employer({email: email})
                .fetch()
                .then(function (user) {
                    successCb({
                        id: user.get('employer_id'),
                        role: user.get('role'),
                        email: email
                    });
                })
                .catch(function (err) {
                    errorCb(err);
                });
        } else {
            errorCb();
        }
    },


    /**
     * Creates salt for hash & returns object with it or false
     * @param pw_hash - the first password hash from client
     * @param successCb - returns object wiht hash & salt
     * @param errorCb - returns false on error
     */
    hash_pw: function (pw_hash, successCb, errorCb) {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(pw_hash, salt, function (err, hash) {
                successCb({
                    hash: hash,
                    salt: salt
                });
                //return err
                if (err) errorCb(false);
            });
            //return error
            if (err) errorCb(false);
        });
    }
};

/***
 * Generates a jwt token with expire date.
 * @param user - user object
 * @param expires - expire date in milliseconds
 * @returns {{token: String, expires: , user: *}}
 */
function genToken(user) {
    var expires = expiresIn(7); // 7 days
    var token = jwt.encode({
        exp: expires
    }, require('./../config/secret')());

    return {
        token: token,
        expires: expires,
        user: user
    };
}

function expiresIn(numDays) {
    var date = new Date();
    return date.setDate(date.getDate() + numDays);
}

/***
 * checks if requested password is correct
 * @param req_hash - password hash from calling function
 * @param db_hash - user password hash from database
 * @param successCb - returns true on success
 * @param errorCb - returns false on error
 */
function compare_pw(req_hash, db_hash, successCb, errorCb) {
    bcrypt.compare(req_hash, db_hash, function (err, res) {
        if (res) {
            successCb(res);
        } else {
            errorCb(err);
        }
    });
}


module.exports = auth;