/***
 * This module handles login & registering a user.
 * It uses bookshelf to handle DB calls
 *
 * @type {*|exports|module.exports}
 */
var Bookshelf = require('./../bookshelf');
var express = require('express');

var Basic = require('./basic');

var Product = require('./../models/product');
var Products = Bookshelf.Collection.extend({model: Product});
var Finance = require('./../models/finances');
var MaterialCost = require('./../models/materialcost');

var product = {
    getAll: function (req, res) {
        Products.forge().fetch().then(function (products) {
            Basic.respondToClient(res, 200, "Erfolg", "Produkte geladen", null, products);
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Produkte nicht laden", err, null);
        });
    },

    getAllByFilter: function (req, res) {
        var f1 = req.params.f1;
        var f2 = req.params.f2;

        if(f1 === "Preis" && f2 === "absteigend") {
            Products.forge().query(function(qb){
                qb.orderBy('retail_price','DESC');
            }).fetch().then(function (prod) {
                Basic.respondToClient(res, 200, null, null, null, prod);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Produkte Filter nicht anwenden", err, null);
            });
        } else if (f1 === "Preis" && f2 === "aufsteigend") {
            Products.forge().query(function(qb){
                qb.orderBy('retail_price','ASC');
            }).fetch().then(function (prod) {
                Basic.respondToClient(res, 200, null, null, null, prod);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Produkte Filter nicht anwenden", err, null);
            });

        } else if (f1 === "Anzahl" && f2 === "absteigend") {
            Products.forge().query(function(qb){
                qb.orderBy('count','DESC');
            }).fetch().then(function (prod) {
                Basic.respondToClient(res, 200, null, null, null, prod);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Produkte Filter nicht anwenden", err, null);
            });

        } else if (f1 === "Anzahl" && f2 === "aufsteigend") {
            console.error("COUNT ASC");
            Products.forge().query(function(qb){
                qb.orderBy('count','ASC');
            }).fetch().then(function (prod) {
                Basic.respondToClient(res, 200, null, null, null, prod);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Produkte Filter nicht anwenden", err, null);
            });

        } else {
            Basic.respondToClient(res, 404, "Falscher Suchparameter", "Der eingegebene Suchparamter ist falsch", null, null);
        }

    },

    getOne: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Product Data
             */
            Product.forge({'product_id': id}).fetch()
                .then(function (product) {
                    Basic.respondToClient(res, 200, null, null, null, product);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden des Produktes", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Produkt nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        //may check if product exist
        var product = req.body.product;
        var materialCost = req.body.product.material_cost;

        if (product.product_name && product.category &&
            product.retail_price && product.purchase_price && product.count) {

            new Product().save({
                product_name: product.product_name,
                category: product.category,
                retail_price: product.retail_price,
                purchase_price: product.purchase_price,
                count: product.count
            }).then(function (product) {
                var productID = product.get('product_id');

                MaterialCost.forge().save({
                    product_id: productID,
                    sum: materialCost,
                    date: new Date()
                }).then(function () {
                    Basic.respondToClient(res, 200, "Erfolg", "Materialkosten aktualisiert", null, product);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Materialkosten nicht speichern", err, null);
                });
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Finanzen nicht aktualisieren", null, null);
            });

        } else {
            Basic.respondToClient(res, 500, "Fehler", "Bitte f�lle alle Felder komplett aus.", {err: "alle Felder ausf�llen"}, null);
        }

        console.log(product);
    },

    update: function (req, res) {
        var id = req.params.id;
        var product = req.body.product;

        if (product.product_name && product.category &&
            product.retail_price && product.purchase_price && product.count) {

            if (id) {

                new Product({product_id: id})
                    .save({
                        product_name: product.product_name,
                        category: product.category,
                        retail_price: product.retail_price,
                        purchase_price: product.purchase_price,
                        count: product.count
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                    });

            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Produkt nicht speichern", null, null);
            }
        } else {
            Basic.respondToClient(res, 403, "Fehler", "Formular wurde nicht komplett ausgef�llt", null, null);
        }
    },

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            Product.forge({product_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Produkt erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Produkt nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Produkt nicht loeschen", null, null);
        }
    },

    top5: function(req, res) {
        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */

        getTop5().then(function (topP) {
            Basic.respondToClient(res, 200, null, null, null, topP);
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Moante nicht laden", err, null);
        });

        function getTop5() {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT product_name, SUM(amount) AS amount ' +
                    'FROM order_items JOIN product USING (product_id) ' +
                    'GROUP BY product_id ORDER BY SUM(amount) DESC')
                    .then(function (response) {
                        var tmp = [];
                        var labels = [];
                        response[0].forEach(function (item) {
                            tmp.push(item.amount);
                            labels.push(item.product_name);
                        });
                        resolve(new Array(tmp, labels));
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }
    }

};


module.exports = product;