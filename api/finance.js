/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');
var Promise = require('bluebird');

var Employer = require('./../models/employer');
var Employers = Bookshelf.Collection.extend({model: Employer});
var Customer = require('./../models/customer');

var Delivers = require('./../models/delivers');
var Order = require('./../models/orders');
var OrderItem = require('./../models/orderItems');
var Product = require('./../models/product');
var Vehicle = require('./../models/vehicle');

var MaterialCost = require('./../models/materialcost');
var VehicleCost = require('./../models/vehiclecost');
var FuelCost = require('./../models/fuelcost');
var EmployerCost = require('./../models/employercost');
var Tour = require('./../models/tours');

var Finance = require('./../models/finances');
var Fix = require('./../models/fixcost');

var finance = {

    //estimates costs for date today
    estimateDate: function (req, res) {
        var date = req.body.date;
        Basic.parseDate(date);

        //parse date to only DATE without time
        var errors = [];
        var responseData = {};

        getMaterialCost(date).then(function (mtCostSum) {
            responseData.material_cost = mtCostSum;

            getVehicleCost(date).then(function (vhCostSum) {
                responseData.vehicle_cost = vhCostSum;

                getFuelCost(date).then(function (fCostSum) {
                    responseData.fuel_cost = fCostSum;

                    getEmployerCost(date).then(function (eCostSum) {
                        responseData.employer_cost = eCostSum;

                        getSales(date).then(function (salesSum) {
                            responseData.sales = salesSum;

                            Fix.forge({id: 12}).fetch().then(function (fix) {
                                responseData.fix_cost = fix.get('sum');
                                responseData.profit = responseData.sales - (responseData.material_cost +
                                    responseData.fuel_cost + responseData.employer_cost +
                                    responseData.vehicle_cost + responseData.fix_cost);

                                Basic.respondToClient(res, 200, null, null, null, responseData);

                            });


                        }).catch(function (err) {
                            Basic.respondToClient(res, 500, "Fehler", "Konnte Umsatz nicht von Touren ermitteln", err, null);
                        });

                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Mitarbeiterksoten nicht sammeln", err, null);
                    });

                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Benzinkosten nicht sammeln", err, null);
                });

            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeugkosten nicht sammeln", err, null);
            });

        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Materialkosten nicht sammeln", err, null);
        });

        /**
         * Get MaterialCosts for a date
         */
        function getMaterialCost(date) {
            var materialCostSum = 0;
            var promises = [];

            return new Promise(function (resolve, reject) {
                /**
                 * Make SQL QUERY and return data as number!
                 */
                Bookshelf.knex.raw('SELECT SUM(sum) AS mtSum FROM test.material_cost WHERE date = \'' + date + '%\'')
                    .then(function (materialCOST) {
                        if (materialCOST[0][0].mtSum) {
                            resolve(materialCOST[0][0].mtSum);
                        } else {
                            resolve(0);
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Get MaterialCosts for a date
         */
        function getVehicleCost(date) {
            var vehicleCost = 0;
            var promises = [];

            return new Promise(function (resolve, reject) {

                VehicleCost.forge()
                    .query("where", Bookshelf.knex.raw("DATE(date)"), "=", Basic.formatDateOnly(date))
                    .fetchAll({columns: ['sum']})
                    .then(function (costs) {
                        if (costs.length > 0) {

                            promises.push(costs.forEach(function (vehC) {
                                vehicleCost += vehC.get('sum');
                            }));

                            Promise.all(promises).then(function (e) {
                                resolve(vehicleCost);
                            });
                        } else {
                            resolve(0);
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Get FuelCost for a date
         */
        function getFuelCost(date) {
            var fuelCostSum = 0;
            var promises = [];

            return new Promise(function (resolve, reject) {

                FuelCost.forge()
                    .query("where", Bookshelf.knex.raw("DATE(date)"), "=", Basic.formatDateOnly(date))
                    .fetchAll({columns: ['sum']})
                    .then(function (fCost) {
                        if (fCost.length > 0) {

                            promises.push(fCost.forEach(function (fc) {
                                fuelCostSum += fc.get('sum');
                            }));

                            Promise.all(promises).then(function (e) {
                                resolve(fuelCostSum);
                            });
                        } else {
                            resolve(0);
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Get MaterialCosts for a date
         */
        function getEmployerCost(date) {
            var employerCost = 0;
            var promises = [];

            return new Promise(function (resolve, reject) {

                EmployerCost.forge()
                    .query("where", Bookshelf.knex.raw("DATE(date)"), "=", Basic.formatDateOnly(date))
                    .fetchAll({columns: ['sum']})
                    .then(function (costs) {
                        if (costs.length > 0) {

                            promises.push(costs.forEach(function (emC) {
                                employerCost += emC.get('sum');
                            }));

                            Promise.all(promises).then(function (e) {
                                resolve(employerCost);
                            });
                        } else {
                            resolve(0);
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Get MaterialCosts for a date
         */
        function getSales(date) {
            var salesSum = 0;
            var promises = [];

            return new Promise(function (resolve, reject) {
                Tour.forge()
                    .query("where", Bookshelf.knex.raw("DATE(date)"), "=", Basic.formatDateOnly(date))
                    .fetchAll({columns: ['money']})
                    .then(function (tourMoney) {
                        if (tourMoney.length > 0) {
                            promises.push(tourMoney.forEach(function (money) {
                                salesSum += money.get('money');
                            }));

                            Promise.all(promises).then(function (e) {
                                resolve(salesSum);
                            });
                        } else {
                            resolve(0);
                        }
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }


    },

    create: function (req, res) {
        var finance = req.body.finance;
        var date = finance.date.toString();

        if (finance) {
            Finance.forge().where(Bookshelf.knex.raw("DATE(date)"), "=", date)
                .fetch()
                .then(function (fc) {
                    fc.save({
                        material_cost: finance.material_cost,
                        fuel_cost: finance.fuel_cost,
                        employer_cost: finance.employer_cost,
                        vehicle_cost: finance.vehicle_cost,
                        sales: finance.sales,
                        profit: finance.profit
                    }, {method: 'update'}).then(function (f) {
                        Basic.respondToClient(res, 200, "Danke", "Finanzen für " + date +
                            " erforglreich aktualisiert", null, null);
                    });
                }).catch(function (err) {
                    Finance.forge().save({
                        material_cost: finance.material_cost,
                        fuel_cost: finance.fuel_cost,
                        employer_cost: finance.employer_cost,
                        vehicle_cost: finance.vehicle_cost,
                        sales: finance.sales,
                        profit: finance.profit,
                        date: finance.date
                    }).then(function (f) {
                        Basic.respondToClient(res, 200, "Danke", "Finanzen für " + date +
                            " erforglreich eingetragen", null, null);
                    });
                });
        } else {
            Basic.respondToClient(res, 404, "Fehler", "Einige Finanzdaten fehlen!", null, null);
        }
    },

    allMonth: function (req, res) {
        var responseData = {};
        responseData.data = [];
        responseData.labels = [];
        var promises = [];
        var dt = req.body.date;

        promises.push(sumMaterialCost(dt));
        promises.push(sumFuelCost(dt));
        promises.push(sumEmployerCost(dt));
        promises.push(sumVehicleCost(dt));
        promises.push(sumSales(dt));
        promises.push(sumProfit(dt));

        Promise.all(promises).then(function (data) {
            responseData.data.push([data[0], data[1], data[2], data[3], data[4], data [5]]);
            responseData.labels = ["Wareneinsatzkosten", "Benzinkosten", "Mitarbeiterkosten",
                "Fahrzeugkosten", "Umsatz", "Gewinn"];
            console.log(data);
            Basic.respondToClient(res, 200, "Erfolg", "Daten geladen", null, responseData);
        });

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumMaterialCost(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(material_cost) AS material FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].material)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumVehicleCost(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(vehicle_cost) AS vehicle FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].vehicle)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumFuelCost(dt) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(fuel_cost) AS fuel FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].fuel)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumEmployerCost(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(employer_cost) AS employer FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].employer)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumSales(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(sales) AS sales FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].sales)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumProfit(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT SUM(profit) AS profit FROM finance WHERE MONTH(date) = MONTH(\'' + Basic.formatDateOnly(dt) + '\')')
                    .then(function (sum) {
                        resolve(sum[0][0].profit)
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

    },


    setFix: function (req, res) {
        var fixCost = req.body.fix;
        console.error(fixCost);

        Fix.forge({id: 12}).save({
            sum: fixCost
        }).then(function (e) {
            Basic.respondToClient(res, 200, "Danke", "Fixkosten wurden aktualisiert", null, null);
        }).catch(function (err) {
            Basic.respondToClient(res, 200, "Fehler", "Konnte Fixkosten nicht speichern", null, null);
        });
    },

    getFix: function (req, res) {

        Fix.forge({id: 12}).fetch()
            .then(function (fixcost) {
                Basic.respondToClient(res, 200, null, null, null, fixcost);
            }).catch(function (err) {
                Basic.respondToClient(res, 200, "Fehler", "Konnte Fixkosten nicht speichern", null, null);
            });

    },

    possibleAll: function (req, res) {
        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */

        getPossibleMonth().then(function (dates) {
            Basic.respondToClient(res, 200, null, null, null, dates);
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Moante nicht laden", err, null);
        });

        function getPossibleMonth() {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT date FROM finance A GROUP BY MONTH(date)')
                    .then(function (response) {
                        var tmp = [];
                        response[0].forEach(function (item) {
                            tmp.push(item.date.toLocaleDateString());
                        });
                        resolve(tmp);
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

    },

    allYear: function (req, res) {
        var responseData = {};
        responseData.data = [];
        responseData.labels = [];
        responseData.series = ["Umsatz", "Gewinn"];
        var promises = [];
        var dt = req.body.date;

        promises.push(sumSales(dt));

        Promise.all(promises).then(function (data) {

            responseData.data.push(data[0], data[1]);
            responseData.labels = ["Januar", "Februar", "März",
                "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
            console.log(data);
            Basic.respondToClient(res, 200, "Erfolg", "Daten geladen", null, responseData);
        });

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function sumSales(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT sales as sales, profit as profit FROM finance WHERE YEAR(\''+Basic.removeOffset(date)+'\') <= YEAR(date) GROUP BY MONTH(date)')
                    .then(function (sum) {
                        var sales = [];
                        var profit = [];
                        sum[0].forEach(function (item) {
                            sales.push(item.sales);
                            profit.push(item.profit);
                            console.log(item);
                        });
                        resolve(new Array(sales, profit));
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }
    },

    possibleAllYear: function (req, res) {
        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */

        getPossibleMonth().then(function (dates) {
            Basic.respondToClient(res, 200, null, null, null, dates);
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Moante nicht laden", err, null);
        });

        function getPossibleMonth() {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT date FROM finance A GROUP BY YEAR(date)')
                    .then(function (response) {
                        var tmp = [];
                        response[0].forEach(function (item) {
                            tmp.push(item.date.toLocaleDateString());
                        });
                        resolve(tmp);
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

    }

};

module.exports = finance;
