/***
 * This module handles gives basic function
 * which are often needed
 *
 * @type {*|exports|module.exports}
 */
var express = require('express');
var Customer = require('./../models/customer.js');
var knex = require('knex');


var basic = {

    /**
     * returns customer ID
     * @param email
     */
    getCustomerByEmail: function (email) {
        Customer.forge({email: email}).fetch().then(function (customer) {
            return customer.get('customer_id');
        });
    },

    /**
     * Simple helper for responding to client
     * @param res
     * @param status
     * @param message
     * @param err
     * @param data
     * @param title
     */
    respondToClient: function (res, status, title, message, err, data) {
        res.status(status);
        res.json({
            title: title,
            message: message,
            err: err,
            data: data
        });
    },

    formatDateOnly: function (date) {
        console.error(new Date(date).toISOString());
        return new Date(date).toISOString();
    },

    removeOffset: function (date) {
        date.toString();
        date = new Date(date);
        // undo the timezone adjustment we did during the formatting
        date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
        // we just want a local date in ISO format
        return date.toISOString().substring(0, 10);
    },

    parseDate: function (date) {
        date.toString();
        date = new Date(date);
        date.toISOString().substring(0, 10);
        return date.toString();
    }

};

module.exports = basic;