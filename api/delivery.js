/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');

var Delivery = require('./../models/delivers');
var Deliverys = Bookshelf.Collection.extend({model: Delivery});
var Order = require('./../models/orders');


var tour = {
        /***
         * Gibt alle Vehicles zurück
         * @param successCb
         * @param errorCb
         */
        getAll: function (req, res) {
            Deliverys.forge().fetch()
                .then(function (tours) {
                    Basic.respondToClient(res, 200, null, null, null, (tours));
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Tour laden", err, null);
                });
        },

        getOne: function (req, res) {
            var id = req.params.id;

            if (id) {
                /**
                 * Get Tour Data
                 */
                Delivery.forge({'tour_id': id})
                    .fetch()
                    .then(function (tour) {
                        Basic.respondToClient(res, 200, null, null, null, tour);
                    })
                    .catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Tour", err, null);
                    });
            } else {
                Basic.respondToClient(res, 500, "Fehler", "Kann Tour nicht laden. Keine ID erkannt", null, null);
            }
        },

        create: function (req, res) {
            var delivery = req.body.delivery;
            var orders = req.body.delivery.orders;
            var promises = [];

            if (delivery && delivery.vehicle_id && delivery.employer_id && delivery.orders) {
                orders.forEach(function (singleOrder) {
                    //Delivery insert
                    promises.push(Delivery.forge().save({
                        order_id: singleOrder.order_id,
                        employer_id: delivery.employer_id,
                        vehicle_id: delivery.vehicle_id
                    }));
                    //Orders Status Change
                    promises.push(Order.forge({order_id: singleOrder.order_id}).save({
                        status: "in Bearbeitung"
                    }));
                });
                Promise.all(promises).then(function () {
                    Basic.respondToClient(res, 200, "Erfolg", "Tour erfolgreich angelegt.", null, null);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Tour nicht anlegen.", err, null);
                });
            } else {
                Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte Felder ausfuellen", {error: ""}, null);
            }
        },

        update: function (req, res) {
            var id = req.params.id;
            var tour = req.body.tour;

            if (tour && tour.employer_id && tour.miles && tour.date && tour.money) {
                if (id) {

                    new Delivery({tour_id: id})
                        .save({
                            employer_id: tour.employer_id,
                            vehicle_id: tour.vehicle_id,
                            order_id: tour.order_id,
                            date: tour.date,
                            miles: tour.miles,
                            money: tour.money
                        }).then(function () {
                            Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                        }).catch(function (err) {
                            Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                        });

                } else {
                    Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Tour nicht speichern", null, null);
                }
            } else {
                Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgefuellt", null, null);
            }

        }
        ,

        delete: function (req, res) {
            var id = req.params.id;
            if (id) {
                Delivery.forge({tour_id: id}).destroy().then(function () {
                    Basic.respondToClient(res, 200, "Erfolg", "Tour erfolgreich entfernt", null, null);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Tour nicht loeschen", err, null);
                });
            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Tour nicht loeschen", null, null);
            }
        },

        getByEmployer: function (req, res) {
            var id = req.params.id;
            console.log(id);
            if (id) {
                new Delivery().where('employer_id', id).fetchAll().then(function (tours) {
                    Basic.respondToClient(res, 200, "Erfolg", null, null, tours);
                }).catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Touren nicht laden", err, null);
                });
            } else {
                Basic.respondToClient(res, 404, "Fehler", "ID nicht angegeben. Konnte Tour pro MItarbeiter nicht laden", {error: ""}, null)
            }
        }


    }
    ;

module.exports = tour;
