/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */

/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');

var Tour = require('./../models/tours');
var Tours = Bookshelf.Collection.extend({model: Tour});
var TourData = require('./../models/tour_data');
var Employer = require('./../api/employer');


var tour = {
    /***
     * Gibt alle Vehicles zurück
     * @param successCb
     * @param errorCb
     */
    getAll: function (req, res) {
        Tours.forge({}).fetch()
            .then(function (tours) {
                Basic.respondToClient(res, 200, null, null, null, (tours));
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Tour laden", err, null);
            });
    },

    getAllTours: function (req, res) {
        var responseData = {};
        responseData.data = [];
        responseData.labels = [];
        var promises = [];
        var dt = req.body.date;

        promises.push(getAllTours());

        Promise.all(promises).then(function (data) {

            responseData.data = data[0];
            //responseData.series = ["Kilometer", "Bargeld"];
            responseData.labels = data[1];
            console.error(date);

            Basic.respondToClient(res, 200, "Erfolg", "Daten geladen", null, responseData);
        });

        /**
         * Sum materialCost by Date
         * @returns {bluebird|exports|module.exports}
         */
        function getAllTours(date) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT money, date FROM tours GROUP BY DAY(date)')
                    .then(function (sum) {
                        var tmp = [];
                        var date = [];
                        sum[0].forEach(function (item) {
                           tmp.push(item.money);
                            date.push(item.date);
                        });
                        resolve(new Arra(tmp, date));
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }
    },

    getOne: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Tour Data
             */
            Tour.forge({'tour_id': id})
                .fetch()
                .then(function (tour) {
                    Basic.respondToClient(res, 200, null, null, null, tour);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Tour", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Tour nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        var tour = req.body.tour;

        if (tour && tour.vehicle_id && tour.miles && tour.date && tour.employer_id, tour.money) {
            /**
             * Validate Customer
             */
            Tour.forge().save({
                employer_id: tour.employer_id,
                vehicle_id: tour.vehicle_id,
                order_id: tour.order_id,
                date: tour.date,
                miles: tour.miles,
                money: tour.money
            }).then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Tour erfolgreich angelegt.", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Tour nicht anlegen.", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehlende Daten", "Bitte Felder ausfuellen", {error: ""}, null);
        }
    },

    update: function (req, res) {
        var id = req.params.id;
        var tour = req.body.tour;

        if (tour && tour.employer_id && tour.miles && tour.date && tour.money) {
            if (id) {

                new Tour({tour_id: id})
                    .save({
                        employer_id: tour.employer_id,
                        vehicle_id: tour.vehicle_id,
                        order_id: tour.order_id,
                        date: tour.date,
                        miles: tour.miles,
                        money: tour.money
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                    });

            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Tour nicht speichern", null, null);
            }
        } else {
            Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgefuellt", null, null);
        }

    }
    ,

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            Tour.forge({tour_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Tour erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Tour nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Tour nicht loeschen", null, null);
        }
    },

    getByEmployer: function (req, res) {
        var id = req.params.id;
        var promises = [];

        getTourID(id).then(function (tourIDs) {
            tourIDs.forEach(function (tour) {
                console.error(tour);
                promises.push(getTour(tour));
            });

            Promise.all(promises).then(function(respond) {
                Basic.respondToClient(res, 200, null, null, null, respond);
            }).catch(function (err){
                console.log(err);
            });

        }).catch(function (err) {
            Basic.respondToClient(res, 500, "fehler", "konnte tour id nicht laden", err, null);
        });



        function getTour(tourID) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT * FROM tours WHERE tour_id ='+tourID)
                    .then(function (tour) {
                        resolve(tour[0][0]);
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

        function getTourID(employerID) {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT tour_id FROM tour_data WHERE employer_id ='+employerID)
                    .then(function (tour) {
                        var tmp = [];
                        tour[0].forEach(function (item) {
                            tmp.push(item.tour_id);
                            console.log(item.tour_id);
                        });
                        resolve(tmp);
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

    }


};

module.exports = tour;
