/***
 * This module handles calls for users
 * @type {*|exports|module.exports}
 */


/**
 * MODELS
 */
var Bookshelf = require('./../bookshelf');

var Auth = require('./auth');
var Basic = require('./basic');
var Promise = require('bluebird');

var Order = require('./../models/orders');
var Orders = Bookshelf.Collection.extend({model: Order});
var Product = require('./../models/product');
var OrderItems = require('./../models/orderItems');
var Vehicle = require('./../models/vehicle');
var Employer = require('./../models/employer');
var Deliver = require('./../models/delivers');
var Tour = require('./../models/tours');
var TourData = require('./../models/tour_data');
var FuelCost = require('./../models/fuelcost');

var DIESELPREIS = 1.23;

/**
 * customer object that contains all customer related DB calls
 *
 * @type {{getAll: Function, getOne: Function, create: Function, update: Function, delete: Function, saveProfile: Function, addToBasket: Function}}
 */
var order = {
    /***
     * Gibt alle Bestellungen zurueck
     * @param successCb
     * @param errorCb
     */
    getAll: function (req, res) {
        var promises = [];
        var errors = [];
        var responseData = [];

        Orders.forge().fetch()
            .then(function (orders) {
                orders.forEach(function (order) {

                    promises.push(Deliver.forge({order_id: order.get('order_id')}).fetch()
                        .then(function (orderDetails) {
                            var employerID = orderDetails.get('employer_id');
                            var vehicleID = orderDetails.get('vehicle_id');

                            order.set('employer_id', employerID);
                            order.set('vehicle_id', vehicleID);

                            responseData.push(order);
                        }).catch(function (err) {
                            responseData.push(order);
                            errors.push(err);
                        }));
                });

                Promise.all(promises).then(function (e) {
                    Basic.respondToClient(res, 200, null, null, errors, (responseData));
                });
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Bestellungen laden", err, null);
            });


    },

    getTouched: function (req, res) {
        var promises = [];
        var errors = [];
        var responseData = [];

        Order.forge().where('status', 'in Bearbeitung').fetchAll()
            .then(function (orders) {
                orders.forEach(function (order) {

                    promises.push(Deliver.forge({order_id: order.get('order_id')}).fetch()
                        .then(function (orderDetails) {
                            var employerID = orderDetails.get('employer_id');
                            var vehicleID = orderDetails.get('vehicle_id');

                            order.set('employer_id', employerID);
                            order.set('vehicle_id', vehicleID);

                            responseData.push(order);
                        }).catch(function (err) {
                            errors.push(err);
                        }));
                });

                Promise.all(promises).then(function (e) {
                    Basic.respondToClient(res, 200, null, null, errors, (responseData));
                });
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Bestellungen laden", err, null);
            });

    },

    getUntouched: function (req, res) {
        Order.forge().where('status', 'offen').fetchAll()
            .then(function (orders) {
                Basic.respondToClient(res, 200, null, null, null, (orders));
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Bestellungen laden", err, null);
            });
    },

    getFinished: function (req, res) {
        var promises = [];
        var errors = [];
        var responseData = [];

        Order.forge().where('status', 'fertig').fetchAll()
            .then(function (orders) {
                orders.forEach(function (order) {

                    promises.push(Deliver.forge({order_id: order.get('order_id')}).fetch()
                        .then(function (orderDetails) {
                            var employerID = orderDetails.get('employer_id');
                            var vehicleID = orderDetails.get('vehicle_id');

                            order.set('employer_id', employerID);
                            order.set('vehicle_id', vehicleID);

                            responseData.push(order);
                        }).catch(function (err) {
                            responseData.push(order);
                            errors.push(err);
                        }));
                });

                Promise.all(promises).then(function (e) {
                    Basic.respondToClient(res, 200, null, null, errors, (responseData));
                });
            })
            .catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte nicht alle Bestellungen laden", err, null);
            });

    },

    getOne: function (req, res) {
        var id = req.params.id;

        if (id) {
            /**
             * Get Customer Data without password & salt
             */
            Order.forge({'order_id': id})
                .fetch()
                .then(function (order) {
                    Basic.respondToClient(res, 200, null, null, null, order);
                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Fehler beim Laden der Bestellung", err, null);
                });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Kann Profildaten nicht laden. Keine ID erkannt", null, null);
        }
    },

    create: function (req, res) {
        var orders = req.body.order;
        var orderLength = orders.basket.length;
        var errors = [];

        if (orders && orders.customer_id && orders.basket && orders.date) {

            Order.forge().save({
                customer_id: orders.customer_id,
                status: "offen",
                order_date: orders.date
            }).then(function (order) {
                var promises = [];
                orders.basket.forEach(function (item) {
                    promises.push(OrderItems.forge().save({
                        order_id: order.id,
                        product_id: item.product_id,
                        amount: item.amount
                    }));
                });
                Promise.all(promises).then(function () {
                    Basic.respondToClient(res, 200, "Vielen Dank", "Ihre Bestellung wurde entgegen genommen");
                }).catch(function (err) {
                    errors.push(err);
                });
            }).catch(function (err) {
                errors.push(err);
            });

            //sequentialAsyncWithEach(orders);

            //Basic.respondToClient(res, 500, "Schade", "Wir konnten deine Bestellung nicht entgegennehmen");

        } else {
            Basic.respondToClient(res, 405, "Keine Bestellungen erhalten", "Bitte wählen Sie Produkte aus!");

            Order.forge().save({
                customer_id: orders.customer_id,
                product_id: order.product_id,
                amount: order.amount,
                status: "offen",
                order_date: orders.date
            }).then(function (newValue) {
                errors.push(newValue);
            }).catch(function (err) {

            });

            Basic.respondToClient(res, 200, "Fehler", "...", errors, null);
        }

    },

    update: function (req, res) {
        var id = req.params.id;
        var order = req.body.order;

        if (order) {
            if (id) {

                new Order({order_id: id})
                    .save({
                        //TODO: ADD DATA
                    }).then(function () {
                        Basic.respondToClient(res, 200, "Erfolg", "Daten wurden erfolgreich gespeichert", null, null);
                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Daten konnten nicht gespeichert werden", err, null);
                    });

            } else {
                Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Bestellung nicht speichern", null, null);
            }
        } else {
            Basic.respondToClient(res, 403, "Fehler", "Pflichtfelder wurde nicht korrekt ausgefuellt", null, null);
        }

    },

    delete: function (req, res) {
        var id = req.params.id;
        if (id) {
            Order.forge({order_id: id}).destroy().then(function () {
                Basic.respondToClient(res, 200, "Erfolg", "Bestellung erfolgreich entfernt", null, null);
            }).catch(function (err) {
                Basic.respondToClient(res, 500, "Fehler", "Konnte Bestellung nicht loeschen", err, null);
            });
        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Bestellung nicht loeschen", null, null);
        }
    },

    detail: function (req, res) {
        var orderID = req.params.id;
        var productsAll = [];
        var data = {};
        var promises = [];


        if (orderID) {
            OrderItems.forge().where('order_id', orderID).fetchAll().then(function (products) {
                //if(products.length > 0) {

                products.forEach(function (item) {
                    //GET ALL PRODUCTS
                    promises.push(Product.forge({product_id: item.product_id}).fetch().then(function (prod) {
                        productsAll.push(prod);
                    }));

                });

                Promise.all(promises).then(function () {
                    data.products = productsAll;

                    Deliver.forge({order_id: orderID}).fetch().then(function (deliver) {
                        var emp = deliver.get('employer_id');
                        var veh = deliver.get('vehicle_id');

                        Vehicle.forge({vehicle_id: veh}).fetch().then(function (vehicle) {
                            data.vehicle = vehicle;
                            Employer.forge({employer_id: emp}).fetch().then(function (employer) {
                                data.employer = employer;

                                Basic.respondToClient(res, 200, "Erfolg", "Daten geladen", null, data);
                            }).catch(function (err) {
                                Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeuge nicht laden", err, null);
                            });
                        }).catch(function (err) {
                            Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeuge nicht laden", err, null);
                        });

                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeuge nicht laden", err, null);
                    });
                });
                //} else {
                //Basic.respondToClient(res, 500, "Orders", "Orders ARRAY < 0", null, null);
                //}

            }).catch(function (err) {
                console.error(err);
            });

        } else {
            Basic.respondToClient(res, 500, "Fehler", "Keine ID angegeben. Konnte Details nicht laden", null, null);
        }
    },

    fulFill: function (req, res) {
        var assign = req.body.assign;
        var orderID = req.params.id;
        var vehicleID = req.body.assign.vehicle_id;
        var employerID = req.body.assign.employer_id;
        var date = req.body.assign.date;

        console.error("ASSIGN DATE FOR TOUR", date);

        //save new Tour
        Tour.forge().save({
            miles: assign.miles,
            money: assign.money,
            date: date
        }).then(function (tour) {
            var tourID = tour.get('tour_id');

            Deliver.forge({order_id: orderID}).fetch({columns: ['delivery_id']})
                .then(function (delivery) {
                    var deliverID = delivery.get('delivery_id');
                    //save tour details in 'macht' relation
                    TourData.forge().save({
                        tour_id: tourID,
                        delivery_id: deliverID,
                        employer_id: employerID
                    }).then(function () {

                        //change order status to 'fertig'
                        Order.forge({order_id: orderID}).save({
                            status: 'fertig'
                        }).then(function () {

                            //get consumption of vehicle for fuel cost calculation
                            Vehicle.forge({vehicle_id: vehicleID}).fetch({columns: ['consumption']})
                                .then(function (consumption) {
                                    var conSumption = consumption.get('consumption');

                                    console.error(((conSumption * DIESELPREIS) / 100) * assign.miles);

                                    //calculate fuel COST and insert data
                                    FuelCost.forge().save({
                                        vehicle_id: vehicleID,
                                        sum: ((conSumption * DIESELPREIS) / 100) * assign.miles,
                                        date: new Date()
                                    }).then(function (e) {
                                        Basic.respondToClient(res, "Bestellung aktualisiert", "Bestellung wurde erfolgreich aktualisiert", null, null);
                                    }).catch(function (err) {
                                        Basic.respondToClient(res, 500, "Fehler", "Konnte Benzinkosten nicht eintragen");
                                    });

                                }).catch(function (err) {
                                   Basic.respondToClient(res, 500, "Fehler", "Konnte Fahrzeug nicht laden", err, null);
                                });



                        }).catch(function (err) {
                            Basic.respondToClient(res, 500, "Fehler", "Konnte Bestellung nicht speichern", err, null);
                        });

                    }).catch(function (err) {
                        Basic.respondToClient(res, 500, "Fehler", "Konnte Tourdetails nicht speichern", err, null);
                    });

                })
                .catch(function (err) {
                    Basic.respondToClient(res, 500, "Fehler", "Konnte Liefer Nr nicht laden", err, null);
                });
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Tour nicht anlegen", err, null);
        });
    },

    myOrders: function (req, res) {
        var id = req.params.id;

        getCustomerOrders().then(function (orders) {
            Basic.respondToClient(res, 200, null, null, null, orders);
        }).catch(function (err) {
            Basic.respondToClient(res, 500, "Fehler", "Konnte Betsellungen nicht laden", err, null);
        });

        function getCustomerOrders() {
            return new Promise(function (resolve, reject) {
                Bookshelf.knex.raw('SELECT order_date,product_name,amount ' +
                    'FROM orders JOIN order_items USING(order_id) ' +
                    'JOIN product USING(product_id) where customer_id ='+id+' ORDER BY order_date DESC')
                    .then(function (response) {
                        resolve(response[0]);
                    }).catch(function (err) {
                        reject(err);
                    });
            });
        }

    }

};


module.exports = order;
