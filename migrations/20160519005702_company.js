exports.up = function (knex, Promise) {
    return Promise.all([

    /***********************
     * KUNDE
     *
     * Entity
     **********************/
        knex.schema.createTableIfNotExists('customer', function (table) {
            table.increments('customer_id').primary();
            table.string('email').notNull();
            table.string('password').notNullable();
            table.string('role').notNull().defaultTo('customer');
            table.string('salt').notNullable();
            table.string('username').notNullable();
            table.string('surname');
            table.string('name');
            table.integer('postcode');
            table.string('city');
            table.string('street');
            table.integer('street_nr');
            table.string('country');
            table.biginteger('tel_nr');
        }),

    /***********************
     * BESTELLUNGEN
     *
     * Entity
     ***********************/
        knex.schema.createTableIfNotExists('orders', function (table) {
            table.increments('order_id').primary();
            table.integer('customer_id');
            table.string('status');
            table.date('order_date');
        }),

    /*********************
     * ENTHAELT
     *
     * Relation => (order_id, product_id) were the composite key
     ***********************/
        knex.schema.createTableIfNotExists('order_items', function (table) {
            table.integer('order_id').unsigned().index().references('order_id').inTable('orders').onDelete('cascade').onUpdate('cascade');
            table.integer('product_id');
            table.string('amount');
        }),

    /*********************
     * PRODUKT
     *
     * Entity
     **********************/
        knex.schema.createTableIfNotExists('product', function (table) {
            table.increments('product_id').primary();
            table.string('product_name');
            table.float('retail_price');                  //Verkaufspreis
            table.integer('count');                         //Anzahl
            table.float('purchase_price');                //Kaufpreisw
            table.string('category');                       //Finanztyp
        }),

    /*********************
     * LIEFERT
     *
     * Relation
     *********************/
        knex.schema.createTableIfNotExists('delivers', function (table) {
            table.increments('delivery_id').primary();
            table.integer('order_id');
            table.integer('employer_id');
            table.integer('vehicle_id');
        }),

    /*********************
     * FAHRZEUG
     *
     * Entity
     *********************/
        knex.schema.createTableIfNotExists('vehicle', function (table) {
            table.increments('vehicle_id').primary();
            table.string('status').notNullable();
            table.string('mark').notNullable();                 //Kennzeichen
            table.string('model');
            table.float('loading_volume').notNullable();      //in Kubikm (m3)
            table.string('brand');                              //Automarke
            table.float('consumption').notNullable();                         //Verbauch auf 100 Km in Litern
            table.float('purchase_price').notNullable();
        }),

    /*********************
     * TOUREN
     *
     * Entity
     *********************/
        knex.schema.createTableIfNotExists('tours', function (table) {
            table.increments('tour_id').primary();
            table.float('miles');                         //in kilometer
            table.double('money');
            table.date('date');
        }),

    /*********************
     * TOUR_DATA
     *
     * Relation
     *********************/
        knex.schema.createTableIfNotExists('tour_data', function (table) {
            table.integer('tour_id').unsigned().index().references('tour_id').inTable('tours').onDelete('cascade').onUpdate('cascade');
            table.integer('delivery_id').unsigned().index().references('delivery_id').inTable('delivers').onDelete('cascade').onUpdate('cascade');
            table.integer('employer_id').unsigned().index().references('employer_id').inTable('employer').onDelete('cascade').onUpdate('cascade');
        }),

    /*********************
     * MITARBEITER
     *
     * Entity
     *********************/
        knex.schema.createTableIfNotExists('employer', function (table) {
            table.increments('employer_id').primary();
            table.string('email').notNull();
            table.string('role').notNull().defaultTo('employer');
            table.string('salt').notNullable();
            table.string('password').notNullable();
            table.string('username').notNullable();
            table.string('name');
            table.string('surname');
            table.biginteger('tel_nr');
            table.integer('postcode');
            table.string('city');
            table.string('country');
            table.string('street');
            table.integer('street_nr');
            table.integer('holidays');
            table.float('target_hours').notNullable();        //Soll-Stunden
            table.float('hourly_wage').notNullable();         //Stundenlohn
        }),

    /*********************
     * Stunden
     *
     * Entity
     *********************/
        knex.schema.createTableIfNotExists('time_estimation', function (table) {
            table.integer('employer_id').unsigned().index().references('employer_id').inTable('employer').onDelete('cascade').onUpdate('cascade');
            table.float('from_hour');
            table.float('to_hour');
            table.date('date');
            table.float('total');
        }),

    /*********************
     * FINANZEN
     *
     * Entity
     *********************/
        knex.schema.createTableIfNotExists('finance', function (table) {
            table.date('date').primary();
            table.double('material_cost');
            table.double('fuel_cost');
            table.double('employer_cost');
            table.double('vehicle_cost');
            table.double('sales');
            table.double('profit');
        }),

        knex.schema.createTableIfNotExists('fix_cost', function (table) {
            table.double('sum');
            table.date('date');
        }),

        knex.schema.createTableIfNotExists('material_cost', function (table) {
            table.integer('product_id').unsigned().index().references('product_id').inTable('product').onDelete('cascade').onUpdate('cascade');
            table.double('sum');
            table.date('date');
        }),

        knex.schema.createTableIfNotExists('fuel_cost', function (table) {
            table.integer('vehicle_id').unsigned().index().references('vehicle_id').inTable('vehicle').onDelete('cascade').onUpdate('cascade');
            table.double('sum');
            table.date('date');
        }),

        knex.schema.createTableIfNotExists('employer_cost', function (table) {
            table.integer('employer_id').unsigned().index().references('employer_id').inTable('employer').onDelete('cascade').onUpdate('cascade');
            table.double('sum');
            table.date('date');
        }),

        knex.schema.createTableIfNotExists('vehicle_cost', function (table) {
            table.integer('vehicle_id').unsigned().index().references('vehicle_id').inTable('vehicle').onDelete('cascade').onUpdate('cascade');
            table.double('sum');
            table.date('date');
        })

    ]);
};

exports.down = function (knex, Promise) {
    return Promise.all([
        //related tables (FK)
        knex.schema.dropTableIfExists('order_items'),
        knex.schema.dropTableIfExists('tour_data'),
        knex.schema.dropTableIfExists('time_estimation'),
        knex.schema.dropTableIfExists('material_cost'),
        knex.schema.dropTableIfExists('fuel_cost'),
        knex.schema.dropTableIfExists('vehicle_cost'),
        knex.schema.dropTableIfExists('employer_cost'),
        knex.schema.dropTableIfExists('fix_cost'),

        //parent tables
        knex.schema.dropTableIfExists('customer'),
        knex.schema.dropTableIfExists('employer'),
        knex.schema.dropTableIfExists('orders'),
        knex.schema.dropTableIfExists('vehicle'),
        knex.schema.dropTableIfExists('tours'),
        knex.schema.dropTableIfExists('finance'),
        knex.schema.dropTableIfExists('product'),
        knex.schema.dropTableIfExists('delivers')
    ]);
};