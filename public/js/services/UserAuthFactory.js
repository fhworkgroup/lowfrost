angular.module('UserAuthFactory', [])

    .factory('UserAuthFactory', function ($window, $location, $http, AuthenticationFactory, config) {
        var domain = config.options.api.base_url;

        return {
            login: function (email, password) {
                return $http.post(domain + '/login', {
                    email: email,
                    password: password
                });
            },
            register: function (user) {
                return $http.post(domain + '/register', {
                    username: user.username,
                    email: user.email,
                    password: user.password
                });
            },
            logout: function () {

                if (AuthenticationFactory.isLogged) {

                    //delete user from factory
                    AuthenticationFactory.isLogged = false;
                    delete AuthenticationFactory.user; //email
                    delete AuthenticationFactory.userRole;
                    delete AuthenticationFactory.userID;

                    //delete token, user and userRole from cache
                    delete $window.sessionStorage.token;
                    delete $window.sessionStorage.user; //email
                    delete $window.sessionStorage.userRole;
                    delete $window.sessionStorage.userID;
                }

            }
        }
    });
