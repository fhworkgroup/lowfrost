angular.module('ModalService', [])

    .factory('ModalService', function ($uibModal) {
        return {
            alertModal: function (title, message) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/LowFrost/public/templates/modals/alert.html',
                    controller: 'ModalInstanceCtrl',
                    size: 'sm'
                });
                modalInstance.title = title;
                modalInstance.message = message;
            },
            confirmModal: function (title, confirmCb, dismissCb) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/LowFrost/public/templates/modals/confirm.html',
                    controller: 'ModalInstanceCtrl',
                    size: 'sm'
                });
                modalInstance.title = title;

                modalInstance.result.then(function () {
                    confirmCb();
                }, function () {
                    dismissCb();
                });
            },
            orderDetails: function (title, data, confirmCb, dismissCb) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/LowFrost/public/templates/modals/orderDetail.html',
                    controller: 'ModalInstanceCtrl',
                    size: 'sm'
                });

                modalInstance.title = title;

                modalInstance.orderItems = data.orderItems;
                modalInstance.vehicle = data.vehicle;
                modalInstance.employer = data.employer;

                modalInstance.result.then(function () {
                    confirmCb();
                }, function () {
                    dismissCb();
                });
            }
        }
    });
