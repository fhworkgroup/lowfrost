angular.module('AuthenticationFactory', [])

.factory('AuthenticationFactory', function($window) {
    var auth = {
        isLogged: false,
        check: function() {
            if ($window.sessionStorage.token && $window.sessionStorage.user) {
                this.isLogged = true;
            } else {
                this.isLogged = false;
                delete this.user;
            }
        },
        isAdmin: function() {
            return isRole("admin");
        },
        isOwner: function() {
            return isRole("owner");
        },
        isAccounting: function() {
            return isRole("accounting");
        },
        isEmployer: function() {
            return isRole("employer");
        },
        isCustomer: function() {
            return isRole("customer");
        }
    };

        /**
         * Can check if param role is true or not
         * @param role
         * @returns {boolean}
         */
        function isRole(role) {
            return (auth.isLogged && auth.userRole === role) ? true : false;
        }


    return auth;
});
