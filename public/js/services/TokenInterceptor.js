angular.module('TokenInterceptor', [])

    .factory('TokenInterceptor', function ($q, $window) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers['X-Access-Token'] = $window.sessionStorage.token;
                    config.headers['X-Key'] = $window.sessionStorage.user;
                    config.headers['X-Role'] = $window.sessionStorage.userRole;
                    config.headers['Content-Type'] = "application/json";
                }
                return config || $q.when(config);
            },

            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if(rejection.status == 0) {
                    window.location = "noresponse.html";
                    return;
                }
                return $q.reject(rejection);
            }
        };
    });