/**
 * Created by chris on 26.05.2016.
 */
angular.module('DataFactory', ['AuthenticationFactory'])

    .factory('DataFactory', function (config, ModalService, AuthenticationFactory, $window) {

        return {
            countries: ['Baden-Wuertemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen',
                ' Hamburg', 'Hessen', 'Mecklenburg-Vorpommern', 'Niedersachsen', 'Nordrhein-Westfalen',
                'Rheinland-Pflaz', 'Saarland', 'Sachsen', 'Sachsen-Anhalt', 'Schleswig-Holstein', 'Thueringen']
            ,
            roles: ['customer', 'owner', 'accounting', 'employer', 'admin'],
            categories: ['Obst', 'Gemuese', 'Wein', 'Pasta', 'Fisch', 'Eis', 'Sonstieges'],
            status: ['unbearbeitet', 'in Bearbeitung', 'Fertig'],
            /**
             * Variable deklaration & Login Status check
             * @type {string}
             */
            api: function () {
                var domain = config.options.api.base_url;
                if (AuthenticationFactory.isLogged) {
                    return domain + config.options.api.api_url + $window.sessionStorage.userRole;
                } else {
                    //ModalService.alertModal("Sorry", "Du bist nicht eingeloggt! Bitte melden Sie sich an, " +
                    //   "um das Programm im vollen Umfang nutzen zu k�nnen");
                    return domain;
                }
            },

            domain: function () {
              return config.options.api.base_url;
            },
            /**
             * Remove date offset from date
             * instantiated with ad Date String
             * @param date
             * @returns {*}
             */
            removeOffset: function (date) {
                date.toString();
                date = new Date(date);
                // undo the timezone adjustment we did during the formatting
                date.setMinutes(date.getMinutes() - date.getTimezoneOffset());
                // we just want a local date in ISO format
                return date.toISOString().substring(0, 10);
            }
        }
    });
