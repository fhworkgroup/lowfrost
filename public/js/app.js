angular.module('fhWikiApi', [
    'ui.bootstrap', 'ui.router',
    'LocalStorageModule',
    'EmployerCtrl', 'CustomerCtrl', 'AuthCtrl', 'ShopCtrl', 'ModalInstanceCtrl',
    'OwnerCtrl',
    'AuthenticationFactory', 'TokenInterceptor', 'UserAuthFactory',
    'DataFactory',
    'chart.js'
])

/***
 * Change domain for development or production
 */
    .constant("config", {
        "options": {
            "api": {
                "base_url": "http://" + window.location.hostname + ":3000",
                "api_url": "/api/v1/",
                "api_url_admin": "admin",
                "api_url_customer": "customer",
                "api_url_employer": "employer",
                "api_url_owner": "owner",
                "api_url_accounting": "accounting"
            }
        },
        "version": "0.0.1"
    })

    .run(function ($rootScope, $window, $state, AuthenticationFactory) {
        // when the page refreshes, check if the user is already logged in
        AuthenticationFactory.check();

        $rootScope.$on("$stateChangeStart", function (event, toState) {
            if (toState.access.requiredLogin && !AuthenticationFactory.isLogged) {
                //not logged redirect to login page
                event.preventDefault();
                $state.go("login");
            }
            // check if user object exists else fetch it. This is incase of a page refresh
            if (!AuthenticationFactory.user) AuthenticationFactory.user = $window.sessionStorage.user;
            if (!AuthenticationFactory.userRole) AuthenticationFactory.userRole = $window.sessionStorage.userRole;
        });

    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.interceptors.push('TokenInterceptor');

        $stateProvider

        /**********************
         * Customer ROUTES
         **********************/
            .state('customer', {
                abstract: true,
                url: '/customers',
                controller: 'CustomerCtrl',
                template: '<ui-view/>'
            })

            .state('customer.index', {
                url: '/index',
                templateUrl: 'templates/customer/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('customer.edit', {
                url: '/edit',
                templateUrl: 'templates/customer/edit.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('customer.profile', {
                url: '/profile',
                templateUrl: 'templates/customer/profile.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('customer.myorders', {
                url: '/profile/myorders',
                templateUrl: 'templates/customer/orders.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('customer.add', {
                url: '/add',
                templateUrl: 'templates/customer/add.html',
                access: {
                    requiredLogin: true
                }
            })

        /***************************
         * Employer ROUTES
         ***************************/

            .state('employer', {
                abstract: true,
                url: '/employers',
                controller: 'EmployerCtrl',
                template: '<ui-view/>'
            })

            .state('employer.index', {
                url: '/index',
                templateUrl: 'templates/employer/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.edit', {
                url: '/edit',
                templateUrl: 'templates/employer/edit.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.profile', {
                url: '/profile',
                templateUrl: 'templates/employer/profile.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.add', {
                url: '/add',
                templateUrl: 'templates/employer/add.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.tours', {
                url: '/tours',
                templateUrl: 'templates/employer/tours/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.tourAdd', {
                url: '/tour/add',
                templateUrl: 'templates/employer/tours/add.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.vehicleUtilization', {
                url: '/vehicles/utilization',
                templateUrl: 'templates/employer/tours/vehicleUtilization.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.drivers', {
                url: '/employers/drivers',
                templateUrl: 'templates/employer/tours/drivers.html',
                access: {
                    requiredLogin: true
                }
            })


            .state('employer.vehicles', {
                url: '/vehicles',
                templateUrl: 'templates/employer/vehicles/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.vehicleAdd', {
                url: '/vehicle/add',
                templateUrl: 'templates/employer/vehicles/add.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.vehicleEdit', {
                url: '/vehicle/edit',
                templateUrl: 'templates/employer/vehicles/edit.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.orders', {
                url: '/orders',
                templateUrl: 'templates/employer/orders/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.editOrder', {
                url: '/orders/edit',
                templateUrl: 'templates/employer/orders/assign.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.hours', {
                url: '/hours',
                templateUrl: 'templates/employer/hours/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.hoursAdd', {
                url: '/hours/add',
                templateUrl: 'templates/employer/hours/add.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.assignments', {
                url: '/assignments',
                templateUrl: 'templates/employer/assignments/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('employer.assignmentFulFill', {
                url: '/assignment/fulfill',
                templateUrl: 'templates/employer/assignments/fulfill.html',
                access: {
                    requiredLogin: true
                }
            })


        /*************************
         * Owner ROUTES
         ************************/
            .state('owner', {
                url: '/owner',
                abstract: true,
                template: '<ui-view/>',
                controller: 'EmployerCtrl'
            })

            .state('owner.tours', {
                url: '/tours',
                templateUrl: 'templates/owner/tours.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('owner.sales', {
                url: '/sales',
                templateUrl: 'templates/owner/sales.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('owner.product', {
                url: '/product',
                templateUrl: 'templates/owner/product.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('owner.customers', {
                url: '/customers',
                templateUrl: 'templates/owner/customers.html',
                access: {
                    requiredLogin: true
                }
            })

        /*************************
         * ACCOUNTING ROUTES
         ************************/
            .state('accounting', {
                url: '/accounting',
                abstract: true,
                template: '<ui-view/>',
                controller: 'EmployerCtrl'
            })

            .state('accounting.index', {
                url: '/finances',
                templateUrl: 'templates/accounting/index.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('accounting.add', {
                url: '/finance/add',
                templateUrl: 'templates/accounting/add.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('accounting.fix', {
                url: '/fixcosts',
                templateUrl: 'templates/accounting/fix.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('accounting.sales', {
                url: '/sales',
                templateUrl: 'templates/accounting/sales.html',
                access: {
                    requiredLogin: true
                }
            })

        /*************************************
         * Static Routes & Login + Register ...
         *************************************/
            .state('login', {
                url: "/login",
                templateUrl: 'templates/auth/login.html',
                controller: 'AuthCtrl',
                access: {
                    requiredLogin: false
                }
            })

            .state('register', {
                url: "/register",
                templateUrl: 'templates/auth/register.html',
                controller: 'AuthCtrl',
                access: {
                    requiredLogin: false
                }
            })

            .state('contact', {
                url: '/kontakt',
                templateUrl: 'templates/contact.html',
                access: {
                    requiredLogin: false
                }
            })


        /******************************
         * Shop routing
         *****************************/
            .state('shop', {
                url: '/shop',
                abstract: true,
                template: '<ui-view/>',
                controller: 'ShopCtrl'
            })

            .state('shop.index', {
                url: "/index",
                templateUrl: 'templates/shop/index.html',
                //controller: 'ShopCtrl',
                access: {
                    requiredLogin: false
                }
            })

            .state('shop.catalog', {
                url: '/catalog',
                templateUrl: 'templates/shop/catalog.html',
                //controller: 'ShopCtrl',
                access: {
                    requiredLogin: false
                }
            })

            .state('shop.products', {
                url: '/products',
                templateUrl: 'templates/shop/product/products.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('shop.productEdit', {
                url: '/productEdit',
                templateUrl: 'templates/shop/product/edit.html',
                access: {
                    requiredLogin: true
                }
            })

            .state('shop.productAdd', {
                url: '/productAdd',
                templateUrl: 'templates/shop/product/add.html',
                access: {
                    requiredLogin: true
                }
            });

        $urlRouterProvider.otherwise("shop/catalog");

    })

    //spinner directive
    .directive("spinner", function () {
        return {
            restrict: 'E',
            scope: {enable: "="},
            template: '<div class="spinner" ng-show="enable"><img src="../images/spinner.gif"></div>'
        }
    })

    .directive('datepickerPopup', function () {
        return {
            restrict: 'EAC',
            require: 'ngModel',
            link: function (scope, element, attr, controller) {
                //remove the default formatter from the input directive to prevent conflict
                controller.$formatters.shift();

            }
        }
    })

    .directive('datepickerLocaldate', ['$parse', function ($parse) {
        var directive = {
            restrict: 'A',
            require: ['ngModel'],
            link: link
        };
        return directive;

        function link(scope, element, attr, ctrls) {
            var ngModelController = ctrls[0];

            // called with a JavaScript Date object when picked from the datepicker
            ngModelController.$parsers.push(function (viewValue) {
                // undo the timezone adjustment we did during the formatting
                viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
                // we just want a local date in ISO format
                return viewValue.toISOString().substring(0, 10);
            });

            // called with a 'yyyy-mm-dd' string to format
            ngModelController.$formatters.push(function (modelValue) {
                if (!modelValue) {
                    return undefined;
                }
                // date constructor will apply timezone deviations from UTC (i.e. if locale is behind UTC 'dt' will be one day behind)
                var dt = new Date(modelValue);
                // 'undo' the timezone offset again (so we end up on the original date again)
                dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
                return dt;
            });
        }
    }]);