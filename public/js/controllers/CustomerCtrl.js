angular.module('CustomerCtrl', ['AuthenticationFactory', 'ModalService', 'DataFactory'])
/***
 * Handles Basic user functions
 */
    .controller('CustomerCtrl', function ($rootScope, $scope, $http, config, $filter, $state, $stateParams,
                                          AuthenticationFactory, $window, ModalService, DataFactory) {
        /**
         * GENERAL VARS
         */
        $rootScope.userdata = {};
        var api = DataFactory.api();
        //Bundesl�nder Daten
        $scope.countries = DataFactory.countries;
        //Rollen
        $scope.roles = DataFactory.roles;

        /***
         * Initializes all Customers
         */
        $scope.getAllCustomers = function () {
            $http({
                url: api + '/customers',
                method: "GET"
            }).then(function (res) {
                $scope.users = res.data.data;
                $scope.userCount = countUsers(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.users = orderBy($scope.users, predicate, $scope.reverse);
                };
                $scope.order('email', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.log(err);
            });
        };

        $scope.getUserById = function (id, cb) {
            $http({
                url: api + '/customer/' + id,
                method: 'GET'
            }).then(function (res) {
                cb(res.data.data);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };

        /***
         * @param userArray
         * @returns {*}
         */
        function countUsers(userArray) {
            return userArray.length;
        }

        /**
         * Edit USERDATA
         * @param id
         */
        $scope.editUser = function (id) {
            $scope.getUserById(id, function (user) {
                $rootScope.userdata.user = user;
            });
            $state.go('customer.edit');
        };

        /***
         * User Profile calls
         */
        $scope.getUserProfile = function () {
            if (!$window.sessionStorage.userID) {
                console.error('Can not get User Profile, because id is undefined');
            } else {
                $http({
                    url: api + '/customer/' + $window.sessionStorage.userID,
                    method: 'GET'
                }).then(function (res) {
                    $rootScope.userdata.user = res.data.data;
                }, function (res) {
                    console.log(res.data);
                    ModalService.alertModal("Beachten Sie", res.data.message);
                });
            }
        };

        $scope.saveUserProfile = function () {
            console.log(api + '/customer/' + $window.sessionStorage.userID);
            var user = $rootScope.userdata.user || {};

            user = {
                username: user.username,
                email: user.email,
                role: user.role,
                surname: user.surname,
                name: user.name,
                country: user.country,
                city: user.city,
                postcode: user.postcode,
                street: user.street,
                street_nr: user.street_nr,
                tel_nr: user.tel_nr
            };

            console.log(user);

            //check if required fields are set
            if (user.surname && user.name && user.country && user.city && user.postcode &&
                user.street && user.street_nr) {
                if (!$window.sessionStorage.userID) {
                    console.error('Can not save User Profile, because id is undefined')
                } else {
                    $http({
                        url: api + '/customer/' + $window.sessionStorage.userID,
                        method: 'PUT',
                        data: {
                            userData: user
                        }
                    }).then(function (res) {
                        ModalService.alertModal(res.data.title, res.data.message);
                    }, function (err) {
                        ModalService.alertModal("Fehler", "Konnte Profildaten nicht speichern.");
                        console.error(err.data);
                    })
                }
            } else {
                ModalService.alertModal("Achtung", "Pflichtfelder nicht ausgefuellt");
            }

        };

        $scope.saveCustomer = function (userdata) {
            var user = userdata.user || {};

            console.log(userdata.user);

            user = {
                username: user.username,
                email: user.email,
                role: user.role,
                surname: user.surname,
                name: user.name,
                country: user.country,
                city: user.city,
                postcode: user.postcode,
                street: user.street,
                street_nr: user.street_nr,
                tel_nr: user.tel_nr
            };

            //check if required fields are set
            if (user.surname && user.name && user.country && user.city && user.postcode &&
                user.street && user.street_nr) {
                if (!$window.sessionStorage.userID) {
                    console.error('Can not save User Profile, because id is undefined')
                } else {
                    $http({
                        url: api + '/customer/' + userdata.user.customer_id,
                        method: 'PUT',
                        data: {
                            userData: user
                        }
                    }).then(function (res) {
                        ModalService.alertModal(res.data.title, res.data.message);
                        $state.go('customer.index');
                    }, function (res) {
                        ModalService.alertModal(res.data.title, res.data.message);
                        console.error(res.data.err);
                    });
                }
            } else {
                ModalService.alertModal("Achtung", "Pflichtfelder nicht ausgefuellt");
            }

        };


        /**
         * Resets the add new Customer Form
         */
        $scope.resetForm = function () {
            $rootScope.userdata.user = "";
            return;
        };

        /**
         * Adds a new customer to the database
         * @param userdata
         */
        $scope.addNewCustomer = function (userdata) {
            if (userdata.user) {
                var user = userdata.user;

                if (user.email && user.username && user.password) {
                    user = {
                        username: user.username,
                        email: user.email,
                        password: sha256(user.password),
                        surname: user.surname,
                        name: user.name,
                        country: user.country,
                        city: user.city,
                        postcode: user.postcode,
                        street: user.street,
                        street_nr: user.street_nr,
                        tel_nr: user.tel_nr
                    };

                    $http({
                        url: api + '/customer',
                        method: 'POST',
                        data: {
                            userData: user
                        }
                    }).then(function (res) {
                        console.log(res);
                        ModalService.alertModal(res.data.title, res.data.message);
                        $state.go('customer.index');

                        //clear form
                        $scope.userdata.user = {};
                    }, function (res) {
                        ModalService.alertModal(res.title, res.message);
                        console.error(res.err);
                    })
                } else {
                    //console.log(user, $scope);
                    ModalService.alertModal("Achtung", "Bitte Pflichtfelder ausfuellen!");
                }
            } else {
                ModalService.alertModal("Achtung", "Bitte Pflichtfelder ausfuellen!");
            }
        };

        $scope.removeCustomer = function (id) {
            ModalService.confirmModal("Kunde wirklich loeschen?", function () {
                $http({
                    url: api + '/customer/' + id,
                    method: 'delete'
                }).then(function (res) {
                    //actually remove item from local array on client
                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].customer_id === id) {
                            $scope.users.splice(i, 1);
                        }
                    }
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                });

            }, function () {
                //nothing to do here
            });
        };

        $scope.getCustomerOrders = function () {
            $http.get(api + '/orders/all/' + $window.sessionStorage.userID || AuthenticationFactory.userID)
                .then(function (res) {
                    $scope.orders = res.data.data;
                    console.log(res.data.data);
                }).catch(function (err) {
                    console.error(err);
                });
        }

    });