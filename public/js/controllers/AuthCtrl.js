angular.module('AuthCtrl', ['LocalStorageModule', 'UserAuthFactory', 'AuthenticationFactory', 'ModalService'])
/***
 * Handles Authentication
 */
    .controller('AuthCtrl', function ($scope, $rootScope, $window, $http, $state,
                                      localStorageService, UserAuthFactory,
                                      AuthenticationFactory, config, ModalService) {
        var domain = config.options.api.base_url;
        var api = config.options.api.api_url + domain;

        $scope.user = {};

        //check if user is already logged
        if (AuthenticationFactory.isLogged && $state.current.url === "/login") {
            $state.go('user.index');
        }

        //login function
        $scope.login = function () {
            var user = {
                email: $scope.user.email,
                password: $scope.user.password //hash password before using
            };

            $scope.isLoading = true;

            if ((user.email && user.password) && (user.email !== "undefined" && user.password !== "undefined")) {
                //actual login. This sends post request
                UserAuthFactory.login(user.email, sha256(user.password))
                    .success(function (data) {
                        ModalService.alertModal("Willkommen", data.message || "Hallo " + data.user.username +
                            ". Schoen dich wieder zu sehen");

                        AuthenticationFactory.isLogged = true;
                        AuthenticationFactory.user = data.user.email;
                        AuthenticationFactory.userRole = data.user.role;
                        AuthenticationFactory.userID = data.user.id;

                        $window.sessionStorage.token = data.token;
                        $window.sessionStorage.user = data.user.email; // to fetch the user details on refresh
                        $window.sessionStorage.userRole = data.user.role; // to fetch the user details on refresh
                        $window.sessionStorage.userID = data.user.id; // to fetch the user details on refresh

                        // redirect to specific view driven by user role
                        switch (data.user.role) {
                            case 'admin':
                                $state.go('employer.index');
                                break;
                            case 'customer':
                                $state.go('shop.catalog');
                                break;
                            case 'accounting':
                                $state.go('accounting.index');
                                break;
                            case 'owner':
                                $state.go('owner.tours');
                                break;
                            case 'employer':
                                $state.go('customer.index');
                                break;
                            default:
                                $state.go('shop.catalog');
                                break;
                        }

                        $scope.isLoading = false;
                    }).error(function (err) {
                        if (err) {
                            $scope.user.email = "";
                            $scope.user.password = "";

                            ModalService.alertModal("Fehler", err.message);
                        }
                        $scope.isLoading = false;
                    });
            } else {
                ModalService.alertModal("Achtung", "Bitte fuelle alle Felder aus!");
                $scope.isLoading = false;
            }
        };

        //logout user
        $scope.logout = function () {
            UserAuthFactory.logout();
            $state.go('login');
        };


        /***
         * Register function
         *
         * @returns {boolean}
         */
        $scope.register = function () {
            var user = {
                username: $scope.user.username,
                email: $scope.user.email,
                password: $scope.user.password,
                password2: $scope.user.password2
            };

            $scope.isLoading = true;

            //check for empty form and password equality
            if (user.username && user.email && user.password && user.password2) {
                if (sha256(user.password) === sha256(user.password2)) {
                    //hash password berfore register
                    user.password = sha256($scope.user.password);
                    //transfrom email to lower case
                    user.email = $scope.user.email.toLowerCase();
                    UserAuthFactory.register(user).then(function successCB(res) {
                        ModalService.alertModal("Willkommen" ,res.data.message + '. Melde dich jetzt an' || "Du kannst dich nun anmelden");

                        //reset form
                        $scope.user.email = "";
                        $scope.user.username = "";
                        $scope.user.password = "";
                        $scope.user.password2 = "";

                        $scope.isLoading = false;

                        $state.go("login");
                    }, function errorCB(res) {
                        ModalService.alertModal("Fehler beim Registrieren", res.data.message || "Konnte Nutzer nicht registrieren");

                        //reset form
                        $scope.isLoading = false;
                        $scope.user.email = "";
                        $scope.user.username = "";
                        $scope.user.password = "";
                        $scope.user.password2 = "";

                        $scope.isLoading = false;
                    });
                } else {
                    ModalService.alertModal("Fehler", "Passwoerter nicht identisch. Versuche es bitte nochmal.");

                    //reset password
                    $scope.user.password = "";
                    $scope.user.password2 = "";

                    $scope.isLoading = false;
                }
            } else {
                ModalService.alertModal("Fehler", "Bitte fuelle alle Felder aus.");

                $scope.isLoading = false;
            }
        };

        /**
         * Uses AuthenticationFactory to check if user is logged
         *
         * return true if logge and false if not
         * @returns {boolean}
         */
        $scope.isLogged = function () {
            return AuthenticationFactory.isLogged;
        };

        $scope.getUserMail = function() {
            return AuthenticationFactory.user;
        };


        /**
         * Here we use AuthenticationFactory to
         * clarify which user role user have
         *
         * each function returns a boolean
         *
         * @return {boolean}
         */
        $scope.isAdmin = function() {
            return AuthenticationFactory.isAdmin();
        };
        $scope.isOwner = function() {
            return AuthenticationFactory.isOwner();
        };
        $scope.isAccounting = function() {
            return AuthenticationFactory.isAccounting();
        };
        $scope.isEmployer = function() {
            return AuthenticationFactory.isEmployer();
        };
        $scope.isCustomer = function() {
            return AuthenticationFactory.isCustomer();
        };

        /***
         * Here we check if the current state is equal to stateName.
         * If yes returns "active" as string
         *
         * can be used to highlight a link as active (class="active")
         *
         * @param stateName - state to check string("/stateName")
         * @returns {string} - "active"
         */
        $scope.isActiveState = function (stateName) {
            if ($state.$current.self.name === stateName) {
                return "active";
            }
        };

    });