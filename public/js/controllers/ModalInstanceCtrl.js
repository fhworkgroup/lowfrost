angular.module('ModalInstanceCtrl', [])
/**
 * This controller serves all functions for a modal
 * and connects the data from a modal template with the modal instance data
 * e.g. title or body text
 */
    .controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {
        /**
         * Template Models from alert.html (/templates/modals/...)
         * @type {{}}
         */
        $scope.alert = {};
        $scope.alert.title = $uibModalInstance.title;
        $scope.alert.message = $uibModalInstance.message;

        $scope.orderItems = $uibModalInstance.orderItems;
        $scope.employer = $uibModalInstance.employer;
        $scope.vehicle = $uibModalInstance.vehicle;


        /**
         * Modal Instance functions
         */
        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });

