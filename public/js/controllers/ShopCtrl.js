angular.module('ShopCtrl', ['DataFactory', 'ModalService', 'AuthenticationFactory'])

    .controller('ShopCtrl', function ($rootScope, $scope, $state, $filter,
                                      $http, DataFactory, ModalService,
                                      AuthenticationFactory, $window) {
        /**
         * Needed vars defined here
         * @type {{}}
         */
        var api = DataFactory.api();
        var domain = DataFactory.domain();
        $rootScope.products = {};
        $scope.basket = [];
        $scope.categories = DataFactory.categories;
        $scope.isLogged = AuthenticationFactory.isLogged;
        $scope.filter1 = ["Preis", "Anzahl"];
        $scope.filter2 = ["aufsteigend", "absteigend"];
        $scope.hasBasket = function () {
            if($scope.basket.length <= 0 || $scope.basket.length == undefined ||
                $scope.basket.length == null) {
                return true;
            } else {
                return false;
            }
        };
        $scope.productFilter1 = "";
        $scope.productFilter2 = "";


        $scope.getAllProductsFilter = function (f1, f2) {
            if(f1 && f2) {
                $http({
                    url: domain + '/product/filter/' + f1 + '/' + f2,
                    method: 'GET'
                }).then(function (res) {
                    $rootScope.products = res.data.data;

                    /**
                     * Search stuff
                     */
                    var orderBy = $filter('orderBy');
                    $scope.order = function (predicate) {
                        $scope.predicate = predicate;
                        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                        $scope.products = orderBy($scope.products, predicate, $scope.reverse);
                    };
                    $scope.order('name', true);

                    console.log(res.data);
                }).catch(function (err) {
                    console.error(err);
                })
            }
        };





        /**********************
         *      PRODUCTS
         **********************/

        $scope.getAllProducts = function () {
            console.log(api + '/products');
            //reset products
            $rootScope.products = "";

            $http({
                url: api + '/products',
                method: 'GET'
            }).then(function (res) {
                $rootScope.products = res.data.data;

                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $rootScope.products = orderBy($rootScope.products, predicate, $scope.reverse);
                };
                $scope.order('name', true);

                $scope.productCount = countProducts($rootScope.products);
            }, function (err) {
                console.error(err)
            });
        };

        $scope.getProductById = function (id, cb) {
            $http({
                url: api + '/product/' + id,
                method: 'GET'
            }).then(function (res) {
                cb(res.data.data);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };

        /***
         * @param products
         * @returns {*}
         */
        function countProducts(products) {
            if (products) return products.length;
        }

        $scope.editProduct = function (id) {
            $scope.getProductById(id, function (product) {
                $rootScope.singleProduct = product;
            });
            $state.go('shop.productEdit');
        };


        $scope.saveProduct = function (product) {
            //var product = $rootScope.singleProduct || product;
            if (product && product.product_name && product.category &&
                product.retail_price && product.purchase_price && product.count) {

                $http({
                    url: api + '/product/' + $rootScope.singleProduct.product_id,
                    method: 'PUT',
                    data: {
                        product: {
                            product_name: product.product_name,
                            retail_price: product.retail_price,
                            count: product.count,
                            purchase_price: product.purchase_price,
                            category: product.category
                        }
                    }
                }).then(function (res) {
                    //ModalService.alertModal(res.data.title, res.data.message);
                    $state.go('shop.products');
                }, function (err) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.error(err.data.err);
                });
            } else {
                console.log(product);
                ModalService.alertModal("Achtung", "Bitte f�lle alle Felder ganz aus");
            }

        };

        $scope.addProduct = function (product) {
            if (product && product.product_name && product.category &&
                product.retail_price && product.purchase_price) {

                $http({
                    url: api + '/product',
                    method: 'POST',
                    data: {
                        product: {
                            product_name: product.product_name,
                            retail_price: product.retail_price,
                            count: product.count,
                            purchase_price: product.purchase_price,
                            category: product.category,
                            material_cost: (product.count * product.purchase_price)
                        }
                    }
                }).then(function (res) {
                    //ModalService.alertModal(res.data.title, res.data.message);
                    $state.go('shop.products');
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.error(res.data.err);
                });

            } else {
                ModalService.alertModal("Achtung", "Bitte fuelle alle Felder aus.");
            }
        };


        $scope.removeProduct = function (id) {
            ModalService.confirmModal("Produkt wirklich loeschen?", function () {
                $http({
                    url: api + '/product/' + id,
                    method: 'delete'
                }).then(function (res) {
                    //actually remove item from local array on client
                    for (var i = 0; i < $scope.products.length; i++) {
                        if ($scope.products[i].product_id === id) {
                            $scope.products.splice(i, 1);
                        }
                    }
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                });

            }, function () {
                //nothing to do here
            });
        };

        /*************************
         *      BASKET
         ************************/

        /**
         * Adds a Produkt to the basket if not already in basket
         * or increments the amount of given product
         * @param id
         * @param prod
         * @param cb
         */
        $scope.addToBasket = function (id, product) {
            product.amount = product.amount || 1;
            product.price = 0;

            for (var i = 0; i < $scope.basket.length; i++) {
                var basketItemId = $scope.basket[i].product_id;
                var basketItem = $scope.basket[i];
                if (basketItemId === id) {
                    basketItem.amount++;
                    basketItem.price += product.retail_price;
                    return;
                }
            }
            $scope.basket.push({
                product_id: product.product_id,
                product_name: product.product_name,
                price: product.price || product.retail_price,
                amount: product.amount,
                retail_price: product.retail_price
            });
        };

        /**
         * This deletes a product from the customer basket
         *
         * @param product
         *
         * deletes Item from basket
         * returns Success or Error as JSON object
         */
        $scope.deleteFromBasket = function (product) {
            var index = $scope.basket.indexOf(product);

            if(index > -1 && ($scope.basket[index].amount > 1) ) {
                $scope.basket[index].amount--;
                var price = $scope.basket[index].price;
                var ret_price = product.retail_price;

                if ( (price - ret_price) != 0 ) {
                    $scope.basket[index].price -= product.retail_price;
                } else {
                    $scope.basket[index].price = product.retail_price;
                }
            } else {
                $scope.basket.splice(index, 1);
            }
        };


        $scope.submitOrder = function (basket) {
            if (basket) {
                $http({
                    url: api + '/order',
                    method: 'POST',
                    data: {
                        order: {
                            customer_id: $window.sessionStorage.userID || AuthenticationFactory.userID,
                            date: new Date(),
                            basket: basket
                        }
                    }
                }).then(function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    $scope.basket = [];
                }, function(res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.error(res.data.err);
                });
            }
        }


    });