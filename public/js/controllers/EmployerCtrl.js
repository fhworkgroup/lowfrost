angular.module('EmployerCtrl', ['AuthenticationFactory', 'ModalService', 'DataFactory'])
/***
 * Handles Basic user functions
 */
    .controller('EmployerCtrl', function ($rootScope, $scope, $http, config, $filter, $state, $stateParams,
                                          AuthenticationFactory, $window, ModalService, DataFactory) {
        /**
         * GENERAL VARS
         */
        $rootScope.userdata = {};
        var api = DataFactory.api();
        //Bundesl�nder Daten
        $scope.countries = DataFactory.countries;
        //Rollen
        $scope.roles = DataFactory.roles;
        $scope.isAdmin = AuthenticationFactory.isAdmin;
        $scope.isAccounting = AuthenticationFactory.isAccounting;
        $scope.isCustomer = AuthenticationFactory.isCustomer;
        $scope.isOwner = AuthenticationFactory.isOwner;
        $scope.status = DataFactory.status;
        $scope.hours = {};
        $rootScope.tourOrders = [];
        $scope.fix = {};

        /***
         * Initializes all Customers
         */
        $scope.getAllEmployers = function () {
            $http({
                url: api + '/employers',
                method: "GET"
            }).then(function (res) {
                $scope.employers = res.data.data;
                $scope.emplyoersCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.employers = orderBy($scope.employers, predicate, $scope.reverse);
                };
                $scope.order('email', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getUserById = function (id, cb) {
            $http({
                url: api + '/employer/' + id,
                method: 'GET'
            }).then(function (res) {
                cb(res.data.data);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };

        /***
         * @param arr
         * @returns {*}
         */
        function countArray(arr) {
            if (arr) return arr.length;
        }

        $scope.editEmployer = function (id) {
            $scope.getUserById(id, function (employer) {
                $rootScope.userdata.employer = employer;
                console.log(employer);
            });
            $state.go('employer.edit');
        };

        /***
         * User Profile calls
         */
        $scope.getUserProfile = function () {
            if (!$window.sessionStorage.userID) {
                console.error('Can not get User Profile, because id is undefined');
            } else {
                $http({
                    url: api + '/employer/' + $window.sessionStorage.userID,
                    method: 'GET'
                }).then(function (res) {
                    $rootScope.userdata.employer = res.data.data;
                }, function (res) {
                    console.log(res.data);
                    ModalService.alertModal("Beachten Sie", res.data.message);
                });
            }
        };

        $scope.saveUserProfile = function () {
            console.log(api + '/employer/' + $window.sessionStorage.userID);
            var employer = $rootScope.userdata.employer || {};

            employer = {
                username: employer.username,
                email: employer.email,
                role: employer.role,
                surname: employer.surname,
                name: employer.name,
                country: employer.country,
                city: employer.city,
                postcode: employer.postcode,
                street: employer.street,
                street_nr: employer.street_nr,
                tel_nr: employer.tel_nr
            };

            //check if required fields are set
            if (employer.surname && employer.name && employer.country && employer.city && employer.postcode &&
                employer.street && employer.street_nr) {
                if (!$window.sessionStorage.userID) {
                    console.error('Can not save User Profile, because id is undefined')
                } else {
                    $http({
                        url: api + '/employer/' + $window.sessionStorage.userID,
                        method: 'PUT',
                        data: {
                            userData: employer
                        }
                    }).then(function (res) {
                        ModalService.alertModal(res.data.title, res.data.message);
                    }, function (err) {
                        ModalService.alertModal("Fehler", "Konnte Profildaten nicht speichern.");
                        console.error(err.data);
                    });
                }
            } else {
                ModalService.alertModal("Achtung", "Pflichtfelder nicht ausgefuellt");
            }

        };

        $scope.saveEmployer = function () {
            var employer = $rootScope.userdata.employer || {};

            employer = {
                username: employer.username,
                email: employer.email,
                role: employer.role,
                hourly_wage: employer.hourly_wage,
                target_hours: employer.target_hours,
                surname: employer.surname,
                name: employer.name,
                country: employer.country,
                city: employer.city,
                postcode: employer.postcode,
                street: employer.street,
                street_nr: employer.street_nr,
                tel_nr: employer.tel_nr
            };

            //check if required fields are set
            if (employer.surname && employer.name && employer.country && employer.city && employer.postcode &&
                employer.street && employer.street_nr) {
                if (!$window.sessionStorage.userID) {
                    console.error('Can not save User Profile, because id is undefined')
                } else {
                    $http({
                        url: api + '/employer/' + $rootScope.userdata.employer.employer_id,
                        method: 'PUT',
                        data: {
                            userData: employer
                        }
                    }).then(function (res) {
                        //ModalService.alertModal(res.data.title, res.data.message);
                        $state.go('employer.index');
                    }, function (err) {
                        ModalService.alertModal("Fehler", "Konnte Profildaten nicht speichern.");
                        console.error(err.data);
                    });
                }
            } else {
                ModalService.alertModal("Achtung", "Pflichtfelder nicht ausgefuellt");
            }

        };

        $scope.addNewEmployer = function (userdata) {
            if (userdata.employer) {
                var employer = userdata.employer;

                if (employer.email && employer.username && employer.password && employer.role &&
                    employer.target_hours && employer.hourly_wage) {
                    employer = {
                        username: employer.username,
                        email: employer.email,
                        password: sha256(employer.password),
                        role: employer.role,
                        hourly_wage: employer.hourly_wage,
                        target_hours: employer.target_hours,
                        surname: employer.surname,
                        name: employer.name,
                        country: employer.country,
                        city: employer.city,
                        postcode: employer.postcode,
                        street: employer.street,
                        street_nr: employer.street_nr,
                        tel_nr: employer.tel_nr
                    };
                    console.log(employer);
                    $http({
                        url: api + '/employer',
                        method: 'POST',
                        data: {
                            userData: employer
                        }
                    }).then(function (res) {
                        console.log("resdata", res);
                        //ModalService.alertModal(res.data.title, res.data.message);
                        $state.go('employer.index');

                        //clear form
                        $scope.userdata.employer = {};
                    }, function (res) {
                        ModalService.alertModal(res.title, res.message);
                        console.error(res.err);
                    })
                } else {
                    console.error(employer);
                    ModalService.alertModal("Achtung", "Bitte alle Pflichtfelder ausfuellen!");
                }
            } else {
                ModalService.alertModal("Achtung", "Bitte Pflichtfelder ausfuellen!");
            }
        };

        $scope.removeEmployer = function (id) {
            ModalService.confirmModal("Mitarbeiter wirklich loeschen?", function () {
                $http({
                    url: api + '/employer/' + id,
                    method: 'delete'
                }).then(function (res) {
                    //actually remove item from local array on client
                    for (var i = 0; i < $scope.employers.length; i++) {
                        if ($scope.employers[i].employer_id === id) {
                            $scope.employers.splice(i, 1);
                        }
                    }
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                });

            }, function () {
                //nothing to do here
            });
        };

        /*****************************
         * FAHRZEUGE
         *****************************/

        $scope.getAllVehicles = function () {
            $http({
                url: api + '/vehicles',
                method: "GET"
            }).then(function (res) {
                $scope.vehicles = res.data.data;
                $scope.vehiclesCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.vehicles = orderBy($scope.vehicles, predicate, $scope.reverse);
                };
                $scope.order('vehicle_id', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getAllVehicleIds = function () {
            $http({
                url: api + '/vehicles/id',
                method: 'GET'
            }).then(function (res) {
                $rootScope.vehicleIDs = res.data.data;
            }, function (res) {
                ModalService.alertModal(res.data.title, res.data.message);
            });
        };

        $scope.addNewVehicle = function (vehicle) {
            if (vehicle && vehicle.mark && vehicle.purchase_price && vehicle.loading_volume) {
                $http({
                    url: api + '/vehicle',
                    method: 'POST',
                    data: {
                        vehicle: {
                            mark: vehicle.mark,
                            purchase_price: vehicle.purchase_price,
                            loading_volume: vehicle.loading_volume,
                            brand: vehicle.brand,
                            model: vehicle.model,
                            consumption: vehicle.consumption
                        }
                    }
                }).then(function (res) {
                    $state.go('employer.vehicles');

                    //clear form
                    $scope.vehicle = {};
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.error(res.err);
                });
            } else {
                console.error(vehicle);
                ModalService.alertModal("Achtung", "Bitte alle Pflichtfelder ausfuellen!");
            }
        };

        $scope.getVehicleById = function (id, cb) {
            $http({
                url: api + '/vehicle/' + id,
                method: 'GET'
            }).then(function (res) {
                cb(res.data.data);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };

        $scope.editVehicle = function (vehicle) {
            $scope.vehicle = vehicle;
            $state.go('employer.vehicleEdit');
        };

        $scope.saveVehicle = function (vehicle) {
            //check if required fields are set
            if (vehicle.mark && vehicle.purchase_price) {
                if (!$window.sessionStorage.userID) {
                    console.error('Can not save Vehicle, because id is undefined')
                } else {
                    $http({
                        url: api + '/vehicle/' + vehicle.vehicle_id,
                        method: 'PUT',
                        data: {
                            vehicle: {
                                mark: vehicle.mark,
                                purchase_price: vehicle.purchase_price,
                                category: vehicle.category,
                                model: vehicle.model,
                                loading_volume: vehicle.loading_volume,
                                consumption: vehicle.consumption
                            }
                        }
                    }).then(function (res) {
                        //ModalService.alertModal(res.data.title, res.data.message);
                        $state.go('employer.vehicles');

                        $rootScope.vehicle = {};
                    }, function (res) {
                        ModalService.alertModal(res.data.title, res.data.message);
                        console.error(res.data);
                    });
                }
            } else {
                ModalService.alertModal("Achtung", "Pflichtfelder nicht ausgefuellt");
            }

        };

        $scope.removeVehicle = function (id) {
            ModalService.confirmModal("Fahrzeug wirklich loeschen?", function () {
                $http({
                    url: api + '/vehicle/' + id,
                    method: 'delete'
                }).then(function (res) {
                    //actually remove item from local array on client
                    for (var i = 0; i < $scope.vehicles.length; i++) {
                        if ($scope.vehicles[i].vehicle_id === id) {
                            $scope.vehicles.splice(i, 1);
                        }
                    }
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                });

            }, function () {
                //nothing to do here
            });
        };


        /****************************
         * TOUREN
         ****************************/

        $scope.getAllDriver = function () {
            $http({
                url: api + '/drivers',
                method: 'get'
            }).then(function (res) {
                $rootScope.drivers = res.data.data;
            }).catch(function (res) {
                console.error(res.data);
            });
        };

        /***
         * Adds a chosen Order to the selected
         * orders section in the view. This is for pre selecting data
         * for the add new Tour request.
         *
         * can be hidden but is made visible.
         * @param order
         */
        $scope.setSelectedOrder = function (order) {
            if (order.isChecked) {
                $rootScope.tourOrders.push(order);
            } else {
                var index = $rootScope.tourOrders.indexOf(order);
                if (index > -1) {
                    $rootScope.tourOrders.splice(index, 1);
                }
            }
        };


        $scope.editOrder = function (tourOrders, vehicleID, employerID) {
            if (tourOrders.length && employerID && vehicleID) {
                $http({
                    url: api + '/delivery/',
                    method: 'POST',
                    data: {
                        delivery: {
                            orders: tourOrders,
                            employer_id: parseInt(employerID),
                            vehicle_id: parseInt(vehicleID)
                        }
                    }
                }).then(function (res) {
                    $rootScope.tourOrders = [];
                    //$rootScope.tours.push(tour);
                    $state.go('employer.orders');
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.log(res.data.err);
                });
            } else {
                ModalService.alertModal("Achtung", "Bitte alles aufuellen");
            }

        };

        $scope.getToursByEmployer = function (id) {
            if(id) {
                $http({
                    url: api + '/tours/byEmployer/' + id,
                    method: 'GET'
                }).then(function (res) {
                    $scope.tours = res.data.data;
                    $rootScope.toursCount = countArray(res.data.data);
                    console.log(res.data.data);
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    console.error(res);
                })
            }
            $http({
                url: api + '/tours/byEmployer/' + $window.sessionStorage.userID || AuthenticationFactory.userID,
                method: 'GET'
            }).then(function (res) {
                $rootScope.tours = res.data.data;
                $rootScope.toursCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.tours = orderBy($scope.tours, predicate, $scope.reverse);
                };
                $scope.order('date', true);
            }, function (res) {
                ModalService.alertModal(res.data.title, res.data.message);
                console.error(res.data);
            })
        };

        $scope.gotoTourAdd = function (assign) {
            $rootScope.assign = assign;
            $state.go('employer.assignmentFulFill');
        };

        $scope.removeTour = function (id) {
            ModalService.confirmModal("Tour wirklich loeschen?", function () {
                console.log($scope.tours);
                $http({
                    url: api + '/tour/' + id,
                    method: 'delete'
                }).then(function (res) {
                    //actually remove item from local array on client
                    for (var i = 0; i < $scope.tours.length; i++) {
                        if ($scope.tours[i].tour_id === id) {
                            $scope.tours.splice(i, 1);
                        }
                    }
                }, function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                });

            }, function () {
                //nothing to do here
            });
        };

        /***********************************
         *          AUFTRÄGE
         **********************************/

        $scope.getAllAssigns = function () {
            var id = AuthenticationFactory.userID || $window.sessionStorage.userID;

            $http({
                url: api + '/assignments/' + id,
                method: 'GET'
            }).then(function (res) {
                $scope.assigns = res.data.data;
                console.log(res.data.data);
            }, function (res) {
                console.error(res.data);
            });
        };


        /***********************************
         * DATEPICKER CODE
         ***********************************/

        $scope.today = function () {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: false
        };

        $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            timeZone: true
        };


        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 7);
        }

        $scope.toggleMin = function () {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };

        $scope.toggleMin();

        $scope.openCalender = function () {
            $scope.popup1.opened = true;
        };


        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        /***************************
         *      BESTELLUNGEN
         **************************/

        $scope.getAllOrders = function () {
            $http({
                url: api + '/orders',
                method: "GET"
            }).then(function (res) {
                $scope.orders = res.data.data;
                $scope.ordersCount = countArray(res.data.data);
                console.log($scope.orders);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.orders = orderBy($scope.orders, predicate, $scope.reverse);
                };
                $scope.order('status', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getUntouchedOrders = function () {
            $http({
                url: api + '/orders/untouched',
                method: "GET"
            }).then(function (res) {
                $scope.orders = res.data.data;
                $scope.ordersCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.orders = orderBy($scope.orders, predicate, $scope.reverse);
                };
                $scope.order('date', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getTouchedOrders = function () {
            $http({
                url: api + '/orders/touched',
                method: "GET"
            }).then(function (res) {
                $scope.orders = res.data.data;
                $scope.ordersCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.orders = orderBy($scope.orders, predicate, $scope.reverse);
                };
                $scope.order('date', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getFinishedOrders = function () {
            $http({
                url: api + '/orders/finished',
                method: "GET"
            }).then(function (res) {
                $scope.orders = res.data.data;
                $scope.ordersCount = countArray(res.data.data);
                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.orders = orderBy($scope.orders, predicate, $scope.reverse);
                };
                $scope.order('date', true);
            }, function (err) {
                //ModalService.alertModal("Fehler beim Laden", err.data.message);
                console.error(err);
            });
        };

        $scope.getOrderById = function (id, cb) {
            $http({
                url: api + '/employer/' + id,
                method: 'GET'
            }).then(function (res) {
                cb(res.data.data);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };

        $scope.orderDetails = function (order) {
            $http({
                url: api + '/order/details/' + order.order_id,
                method: 'GET'
            }).then(function (res) {
                console.log(res.data.data);

                ModalService.orderDetails("Bestelldetails", {
                    orderItems: res.data.data.products,
                    employer: res.data.data.employer,
                    vehicle: res.data.data.vehicle
                }, function () {
                    //CONFIRM
                }, function () {
                    //DISMISS
                });

            }, function (err) {
                console.error(err);
                ModalService.alertModal("Unbearbeitet", "Fuer diese Bestellung liegen keine weiteren informationen vor");
            });
        };


        $scope.fullFillOrder = function (assign) {
            if (assign.miles && assign.money && assign.date) {
                $http({
                    url: api + '/order/fulfill/' + assign.order_id,
                    method: 'POST',
                    data: {
                        assign: {
                            miles: assign.miles,
                            money: assign.money,
                            date: DataFactory.removeOffset(assign.date),
                            vehicle_id: assign.vehicle.vehicle_id,
                            employer_id: (assign.employer_id = AuthenticationFactory.userID || $window.sessionStorage.userID)
                        }
                    }
                }).then(function (res) {
                    console.log("ASSIGN DATA", {
                        miles: assign.miles,
                        money: assign.money,
                        date: DataFactory.removeOffset(assign.date),
                        vehicle_id: assign.vehicle.vehicle_id,
                        employer_id: (assign.employer_id = AuthenticationFactory.userID || $window.sessionStorage.userID)
                    });
                    $state.go('employer.assignments');
                    ModalService.alertModal("Auftrag abgeschlossen", "Bestellung erfolgreich aktualisiert!")
                }, function (res) {
                    console.log(res);
                    if (res.data.err.code === "ER_DUP_ENTRY") {
                        ModalService.alertModal("Achtung", "Für dieses Datum wurde bereits eine Tour gemacht.");
                    }
                    console.error(res);
                });
            } else {
                ModalService.alertModal("Warte", "Bitte fuelle alles aus!");
            }

        };


        /*************************
         *  ADD HOURS TO EMPLOYER
         *************************/
        $scope.addEmployerHours = function (hours) {

            if (hours.from_hour_hh && hours.from_hour_mm &&
                hours.to_hour_hh && hours.to_hour_mm && hours.date) {

                console.log(hours.date, DataFactory.removeOffset(hours.date));
                checkTime(hours, function (t) {
                    $http({
                        url: api + '/hour',
                        method: 'POST',
                        data: {
                            time_estimation: {
                                date: DataFactory.removeOffset(hours.date),
                                from_hour: t.from,
                                to_hour: t.to,
                                total: t.total,
                                employer_id: AuthenticationFactory.userID || $window.sessionStorage.userID
                            }
                        }
                    }).then(function (res) {
                        if (res.status === 201) {
                            ModalService.alertModal(res.data.title, res.data.message);
                            console.log(res.data);
                        } else {
                            $scope.hours = {};
                            $state.go("employer.hours");
                        }
                    }, function (res) {
                        ModalService.alertModal("Sorry", res.data.message);
                        console.log(res.data);
                    });
                }, function () {
                    ModalService.alertModal("Achtung", "Du kannst keine minus Stunden eintragen");
                });


            } else {
                ModalService.alertModal("Bitte Fülle alle Felder aus!");
            }

            /**
             * Calculating the from and to hours to minutes
             * @param hours
             * @returns {{from: *, to: *, total: number}}
             *
             * returns minutes of from, to and total TIME
             */
            function checkTime(hours, resolve, reject) {
                var fh, fm, th, tm;
                fh = parseInt(hours.from_hour_hh);
                fm = parseInt(hours.from_hour_mm);
                th = parseInt(hours.to_hour_hh);
                tm = parseInt(hours.to_hour_mm);

                var from = fh + (fm / 100);
                var to = th + (tm / 100);

                if (to - from <= 0) {
                    reject();
                } else {
                    resolve({
                        from: (from),
                        to: (to),
                        total: (to - from)
                    });
                }
            }
        };

        $scope.getTimeEstimationByEmployer = function () {
            $http({
                url: api + '/hours/' + (AuthenticationFactory.userID || $window.sessionStorage.userID),
                method: 'GET'
            }).then(function (res) {
                $scope.hours.all = res.data.data;
                $scope.hoursCount = 0;
                $scope.hours.all.forEach(function (hour) {
                    $scope.hoursCount += hour.total;
                });

                /**
                 * Search stuff
                 */
                var orderBy = $filter('orderBy');
                $scope.order = function (predicate) {
                    $scope.predicate = predicate;
                    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
                    $scope.hours.all = orderBy($scope.hours.all, predicate, $scope.reverse);
                };
                $scope.order('date', true);
            }, function (err) {
                console.error(err);
                ModalService.alertModal("Fehler", err.data.message);
            });
        };


        /******************************
         * Finanzen
         *****************************/

        $scope.estimateMoneyByNow = function () {
            $scope.estimateMoneyByDate(DataFactory.removeOffset(new Date()));
        };

        $scope.estimateMoneyByDate = function (date) {
            if (date) {
                $http({
                    url: api + '/estimate/date',
                    method: 'POST',
                    data: {
                        date: DataFactory.removeOffset(date)
                    }
                }).then(function (res) {
                    var cost = res.data.data;
                    $scope.cost = {
                        material_cost: cost.material_cost.round(2),
                        fuel_cost: cost.fuel_cost.round(2),
                        employer_cost: cost.employer_cost.round(2),
                        vehicle_cost: cost.vehicle_cost.round(2),
                        sales: cost.sales.round(2),
                        profit: cost.profit.round(2),
                        fix_cost: cost.fix_cost.round(2)
                    };
                    console.log(res.data.data);
                }, function (res) {
                    console.error(res.data);
                })
            } else {
                ModalService.alertModal("Moment", "Bitte gebe ein Datum an");
            }
        };


        $scope.addNewFinance = function (cost) {
            if (cost.material_cost >= 0 && cost.fuel_cost >= 0 && cost.employer_cost >= 0 &&
                cost.sales >= 0 && cost.date) {

                $http({
                    url: api + '/finance',
                    method: 'POST',
                    data: {
                        finance: {
                            material_cost: cost.material_cost,
                            fuel_cost: cost.fuel_cost,
                            employer_cost: cost.employer_cost,
                            vehicle_cost: cost.vehicle_cost,
                            sales: cost.sales,
                            profit: cost.profit,
                            date: DataFactory.removeOffset(cost.date)
                        }
                    }
                }).then(function (res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                    //$state.go('accounting.index');
                }, function (err) {
                    console.error(err);
                })
            } else {
                if (!cost.date) {
                    ModalService.alertModal("Kein Datum", "Bitte Gebe ein Datum an");
                } else {
                    ModalService.alertModal("Felder ausfuellen", "Bitte fuelle alle Felder aus!");
                }
            }
        };


        $scope.calcProfit = function (cost) {
            if (cost) {
                $scope.cost.profit = cost.sales - (cost.material_cost + cost.vehicle_cost +
                    cost.fuel_cost + cost.employer_cost + cost.fix_cost);
            }
        };


        /************************
         * ANGULAR CHART
         *************************/
        /**
         * SETUP CHART JS
         * @type {string[]}
         */
        $scope.costFilters = [];

        $scope.labels = ["January", "February"];
        $scope.series = ['Series A'];
        $scope.data = [
            [10],
            [20]
        ];
        $scope.options = {
            scaleLabel: function (valuePayload) {
                return Number(valuePayload.value).toFixed(2).replace('.', ',') + '€';
            }
            /*scaleOverride: true,
             scaleSteps: 10,
             scaleStepWidth: 1000,
             scaleStartValue: 0*/
        };
        $scope.onClick = function (points, evt) {
            //console.log(points, evt);
        };

        $scope.checkCostFilter = function (f) {
            $scope.getFinancesDate(f);
        };

        /***********************FINANCE REQUEST**********************/
        /*****************************   ****************************/

        /**
         * GET POSSIBLE FINANCE DATES
         */
        $scope.fetchPossibleFinanceDates = function () {
            $http.get(api + '/finance/possible').then(function (res) {
                $scope.costFilters = res.data.data;
            }, function (err) {
                console.error(err);
            })
        };

        /**
         * Get all costs per day of the actual Month
         */
        $scope.getFinancesMonthNow = function () {
            $scope.getFinancesDate(new Date());
        };

        /**
         * fetches Finances by a date
         * @param date
         */
        $scope.getFinancesDate = function (date) {
            $http.post(api + '/finance/cost/month', {
                date: date.toString()
            })
                .then(function (res) {
                    var response = res.data.data;

                    if (response) {
                        $scope.data = response.data;
                        $scope.labels = response.labels;
                        //$scope.series = response.series;
                    }
                }, function (err) {
                    console.error(err);
                });
        };


        /**
         * returns FIXcosts
         */
        $scope.getFixCosts = function () {
            $http.get(api + '/finance/fix').then(function(res) {
                $scope.fix.cost = res.data.data.sum;
            }).catch(function (err) {
                console.error(err);
            })
        };

        /**
         * saves FIXcosts
         */
        $scope.addNewFixCost = function (fix) {
            if(fix) {
                $http.post(api + '/finance/fix', {fix: fix}).then(function(res) {
                    ModalService.alertModal(res.data.title, res.data.message);
                }).catch(function (err) {
                    console.error(err);
                });
            } else {
                ModalService.alertModal("Moment", "Bitte gebe einen Wert in € für die Fixkosten an. (Monatlicher Wert)");
            }

        };

        /******************
         * SALES
         *****************/

        $scope.fetchPossibleSalesDates = function () {
            $http.get(api + '/sales/possible').then(function (res) {
                $scope.costFilters = res.data.data;
            }, function (err) {
                console.error(err);
            })
        };

        /**
         * Get all costs per day of the actual Month
         */
        $scope.getSalesYearNow = function () {
            $scope.getSalesDate(new Date());
        };

        /**
         * fetches Finances by a date
         * @param date
         */
        $scope.getSalesDate = function (date) {
            $http.post(api + '/finance/sales/year', {
                date: date.toString()
            })
                .then(function (res) {
                    var response = res.data.data;
                    if (response) {
                        $scope.data = response.data[0];
                        $scope.labels = response.labels;
                        $scope.series = response.series;
                    }
                    console.log(res.data.data);
                }, function (err) {
                    console.error(err);
                });
        };

        $scope.checkSalesFilter = function (date) {
            $scope.getSalesDate(date);
        };

        /************
         *  SPECIAL JAVASCRIPT
         **********/

        Number.prototype.round = function (p) {
            p = p || 10;
            return parseFloat(this.toFixed(p));
        };

        /**************************************
         * BESITZER
         ********** ***************************/

        $scope.getAllTourData = function () {
            $http.get(api+'/tour/data/all').then(function (res) {
                console.log(res.data.data);
                $scope.data = res.data.data.data;
                $scope.labels = res.data.data.labels;
                $scope.series = res.data.data.series;
            }).catch(function (err) {
                console.error(err);
            })
        };

        $scope.fetchTop5 = function () {
            $http.get(api+ '/top/5').then(function (res) {
                console.log(res.data.data[0]);
                $scope.data = new Array(res.data.data[0]);
                $scope.labels = res.data.data[1];
                $scope.options = {
                    scaleLabel: function (valuePayload) {
                        return Number(valuePayload.value) + 'Stück';
                    }
                    /*scaleOverride: true,
                     scaleSteps: 10,
                     scaleStepWidth: 1000,
                     scaleStartValue: 0*/
                };
            }).catch(function (err) {
                console.error(err);
            })
        }

    });