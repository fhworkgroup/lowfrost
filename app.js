/***
 * Main App Entry Point
 */


/***
 * Dependencies
 * @type {*|exports|module.exports}
 */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var app = express();


// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));


/***
 * Set HEADERS for all routes and restrict
 * all requests to the required domain
 */
app.all('/*', function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS'); // define allowed HTTP verbs
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,X-Role'); // Set custom headers for CORS
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

/***
 * Auth Middleware - This will check if the token is valid
 * will be checked for the token.
 * Any URL's that do not follow the below pattern should be avoided unless you
 * are sure that authentication is not needed
 * Only the requests that start with /api/v1
 */
app.all('/api/v1/*', [require('./middlewares/validateRequest')]);

/***
 * Routing - This will handle all routes to our
 * protected and public routes
 */
app.use('/', require('./routes'));

/***
 * Error Handling
 * @type {string}
 */
//setting environment var
process.env['NODE_ENV'] = 'development';

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to customer
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
});

module.exports = app;