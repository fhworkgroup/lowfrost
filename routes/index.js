/***
 * Routes for the API
 *
 * This Module defines the access level for each user
 * very explicitly.
 *
 * Basic Syntax of domain is:
 * /secure/api/key/ + /userRole/ + /action/ + /parameters/
 *
 * -> secure api key:   is the hidden path which is protected by a middleware,
 *                      that checks for every request if it has an valid jwt Token.
 *
 * -> userRole:         for the purpose of security explicitly each request uri must
 *                      contain a valid userRole to tell the backend & middleware if
 *                      the requested action can be allowed.
 *
 * -> action:           this key specifies which function needs to be used
 *
 * -> parameters:       this api is RESTfull and accepts parameters for some requestss
 *
 * @type {*|exports|module.exports}
 */
var express = require('express');
var router = express.Router();

var auth = require('./../api/auth.js');
var employer = require('./../api/employer.js');
var customer = require('./../api/customer.js');
var product = require('./../api/product');
var vehicle = require('./../api/vehicle');
var tour = require('./../api/tour');
var order = require('./../api/order');
var timeEstimation = require('./../api/time_estimation');
var deliver = require('./../api/delivery');
var finance = require('./../api/finance');



/**
 * Authorization Requests
 * require no login
 */
router.post('/login', auth.login);
router.post('/register', auth.register);

/**
 * No login ROUTES
 */
router.get('/products', product.getAll);
router.get('/product/filter/:f1/:f2', product.getAllByFilter);


/****************************Routes that can be accessed only by authenticated & authorized customer*******************/

/*********************************
 * Admin Routes
 ********************************/
// EDIT CUSTOMER
router.get('/api/v1/admin/customers', customer.getAll);
router.get('/api/v1/admin/customer/:id', customer.getOne);
router.post('/api/v1/admin/customer/', customer.create);
router.put('/api/v1/admin/customer/:id', customer.update);
router.delete('/api/v1/admin/customer/:id', customer.delete);


// EDIT EMPLOYER
router.get('/api/v1/admin/employers', employer.getAll);
router.get('/api/v1/admin/employer/:id', employer.getOne);
router.post('/api/v1/admin/employer/', employer.create);
router.put('/api/v1/admin/employer/:id', employer.update);
router.delete('/api/v1/admin/employer/:id', employer.delete);

router.get('/api/v1/admin/drivers', employer.getDrivers);

// EDIT PRODUCT
router.get('/api/v1/admin/products', product.getAll);
router.get('/api/v1/admin/product/:id', product.getOne);
router.post('/api/v1/admin/product/', product.create);
router.put('/api/v1/admin/product/:id', product.update);
router.delete('/api/v1/admin/product/:id', product.delete);

// EDIT VEHICLE
router.get('/api/v1/admin/vehicles', vehicle.getAll);
router.get('/api/v1/admin/vehicle/:id', vehicle.getOne);
router.post('/api/v1/admin/vehicle/', vehicle.create);
router.put('/api/v1/admin/vehicle/:id', vehicle.update);
router.delete('/api/v1/admin/vehicle/:id', vehicle.delete);
router.get('/api/v1/admin/vehicles/id', vehicle.getVehicleIds);

//TOURS
router.get('/api/v1/admin/tours', tour.getAll);
router.get('/api/v1/admin/tour/:id', tour.getOne);
router.post('/api/v1/admin/tour/', tour.create);
router.put('/api/v1/admin/tour/:id', tour.update);
router.delete('/api/v1/admin/tour/:id', tour.delete);
router.get('/api/v1/admin/tours/byEmployer/:id', tour.getByEmployer);

//EDIT ORDERS
router.get('/api/v1/admin/orders', order.getAll);
router.get('/api/v1/admin/order/:id', order.getOne);
router.post('/api/v1/admin/order/', order.create);
router.put('/api/v1/admin/order/:id', order.update);
router.delete('/api/v1/admin/order/:id', order.delete);

router.get('/api/v1/admin/orders/touched', order.getTouched);
router.get('/api/v1/admin/orders/untouched', order.getUntouched);
router.get('/api/v1/admin/orders/finished', order.getFinished);

router.get('/api/v1/admin/order/details/:id', order.detail);

//TIME ESTIMATION
router.get('/api/v1/admin/hours/:id', timeEstimation.getTimeEstimationByEmployer);
router.post('/api/v1/admin/hour', timeEstimation.create);
router.delete('/api/v1/admin/hour/:id', timeEstimation.delete);

//DELIVERY
router.post('/api/v1/admin/delivery', deliver.create);

//ESTIMATE
router.post('/api/v1/admin/estimate/date', finance.estimateDate);

/***********************************
 * Employer Routes
 ***********************************/
// EDIT CUSTOMER
router.get('/api/v1/employer/customers', customer.getAll);
router.get('/api/v1/employer/customer/:id', customer.getOne);
router.post('/api/v1/employer/customer/', customer.create);
router.put('/api/v1/employer/customer/:id', customer.update);
router.delete('/api/v1/employer/customer/:id', customer.delete);

// EDIT EMPLOYER
router.get('/api/v1/employer/employers', employer.getAll);
router.get('/api/v1/employer/employer/:id', employer.getOne);
router.post('/api/v1/employer/employer/', employer.create);
router.put('/api/v1/employer/employer/:id', employer.update);
router.delete('/api/v1/employer/employer/:id', employer.delete);

router.get('/api/v1/employer/drivers', employer.getDrivers);

// EDIT PRODUCT
router.get('/api/v1/employer/products', product.getAll);
router.get('/api/v1/employer/product/:id', product.getOne);
router.post('/api/v1/employer/product/', product.create);
router.put('/api/v1/employer/product/:id', product.update);
router.delete('/api/v1/employer/product/:id', product.delete);

//VEHICLE
router.get('/api/v1/employer/vehicles', vehicle.getAll);

//TOURS
router.get('/api/v1/employer/tours', tour.getAll);
router.get('/api/v1/employer/tour/:id', tour.getOne);
router.post('/api/v1/employer/tour/', tour.create);
router.put('/api/v1/employer/tour/:id', tour.update);
router.delete('/api/v1/employer/tour/:id', tour.delete);
router.get('/api/v1/employer/tours/byEmployer/:id', tour.getByEmployer);

//ASSIGNMENTS
router.get('/api/v1/employer/assignments/:id', employer.getAssignments);

//EDIT ORDERS
router.get('/api/v1/employer/orders', order.getAll);
router.get('/api/v1/employer/order/:id', order.getOne);
router.post('/api/v1/employer/order/', order.create);
router.put('/api/v1/employer/order/:id', order.update);
router.delete('/api/v1/employer/order/:id', order.delete);

router.get('/api/v1/employer/order/details/:id', order.detail);
router.post('/api/v1/employer/order/fulfill/:id', order.fulFill);

router.get('/api/v1/employer/orders/touched', order.getTouched);
router.get('/api/v1/employer/orders/untouched', order.getUntouched);
router.get('/api/v1/employer/orders/finished', order.getFinished);

//TIME ESTIMATION
router.get('/api/v1/employer/hours/:id', timeEstimation.getTimeEstimationByEmployer);
router.post('/api/v1/employer/hour', timeEstimation.create);

//DELIVERY
router.post('/api/v1/employer/delivery', deliver.create);



/***
 * Owner Routes
 */
// EDIT EMPLOYER
router.get('/api/v1/owner/employers', employer.getAll);
router.get('/api/v1/owner/employer/:id', employer.getOne);
router.post('/api/v1/owner/employer/', employer.create);
router.put('/api/v1/owner/employer/:id', employer.update);
router.delete('/api/v1/owner/employer/:id', employer.delete);

router.get('/api/v1/owner/tours/byEmployer/:id', tour.getByEmployer);
router.get('/api/v1/owner/drivers', employer.getDrivers);

// EDIT PRODUCT
router.get('/api/v1/owner/products', product.getAll);
router.get('/api/v1/owner/product/:id', product.getOne);
router.post('/api/v1/owner/product/', product.create);
router.put('/api/v1/owner/product/:id', product.update);
router.delete('/api/v1/owner/product/:id', product.delete);

router.get('/api/v1/owner/top/5', product.top5);

//EDIT ORDERS
router.get('/api/v1/owner/orders', order.getAll);
router.get('/api/v1/owner/order/:id', order.getOne);
router.post('/api/v1/owner/order/', order.create);
router.put('/api/v1/owner/order/:id', order.update);
router.delete('/api/v1/owner/order/:id', order.delete);

//TIME ESTIMATION
router.get('/api/v1/owner/hours/:id', timeEstimation.getTimeEstimationByEmployer);
router.post('/api/v1/owner/hour', timeEstimation.create);
router.delete('/api/v1/owner/hour', timeEstimation.create);

//FINANCE
router.post('/api/v1/owner/estimate/date', finance.estimateDate);
router.post('/api/v1/owner/finance', finance.create);
router.get('/api/v1/owner/finance/fix', finance.getFix);
router.post('/api/v1/owner/finance/fix', finance.setFix);

router.post('/api/v1/owner/finance/cost/month', finance.allMonth);
router.get('/api/v1/owner/finance/possible', finance.possibleAll);

router.post('/api/v1/owner/finance/sales/year', finance.allYear);
router.get('/api/v1/owner/sales/possible', finance.possibleAllYear);

//SPECIAL
router.get('/api/v1/owner/tour/data/all', tour.getAllTours);



/***********************
 * Accounting Routes:
 ***********************/
// EDIT EMPLOYER
router.get('/api/v1/accounting/employers', employer.getAll);
router.get('/api/v1/accounting/employer/:id', employer.getOne);
router.post('/api/v1/accounting/employer/', employer.create);
router.put('/api/v1/accounting/employer/:id', employer.update);
router.delete('/api/v1/accounting/employer/:id', employer.delete);

// EDIT CUSTOMER
router.get('/api/v1/accounting/customers', customer.getAll);
router.get('/api/v1/accounting/customer/:id', customer.getOne);
router.post('/api/v1/accounting/customer/', customer.create);
router.put('/api/v1/accounting/customer/:id', customer.update);
router.delete('/api/v1/accounting/customer/:id', customer.delete);

// EDIT VEHICLE
router.get('/api/v1/accounting/vehicles', vehicle.getAll);
router.get('/api/v1/accounting/vehicle/:id', vehicle.getOne);
router.post('/api/v1/accounting/vehicle/', vehicle.create);
router.put('/api/v1/accounting/vehicle/:id', vehicle.update);
router.delete('/api/v1/accounting/vehicle/:id', vehicle.delete);

// EDIT PRODUCT
router.get('/api/v1/accounting/products', product.getAll);
router.get('/api/v1/accounting/product/:id', product.getOne);
router.post('/api/v1/accounting/product/', product.create);
router.put('/api/v1/accounting/product/:id', product.update);
router.delete('/api/v1/accounting/product/:id', product.delete);

//EDIT ORDERS
router.get('/api/v1/accounting/orders', order.getAll);
router.get('/api/v1/accounting/order/:id', order.getOne);
router.post('/api/v1/accounting/order/', order.create);
router.put('/api/v1/accounting/order/:id', order.update);
router.delete('/api/v1/accounting/order/:id', order.delete);

//TIME ESTIMATION
router.get('/api/v1/accounting/hours/:id', timeEstimation.getTimeEstimationByEmployer);
router.post('/api/v1/accounting/hour', timeEstimation.create);

//FINANCE
router.post('/api/v1/accounting/estimate/date', finance.estimateDate);
router.post('/api/v1/accounting/finance', finance.create);
router.get('/api/v1/accounting/finance/fix', finance.getFix);
router.post('/api/v1/accounting/finance/fix', finance.setFix);

router.post('/api/v1/accounting/finance/cost/month', finance.allMonth);
router.get('/api/v1/accounting/finance/possible', finance.possibleAll);

router.post('/api/v1/accounting/finance/sales/year', finance.allYear);
router.get('/api/v1/accounting/sales/possible', finance.possibleAllYear);

/**********************************
 * Customer Routes
 **********************************/

//PROFILE
router.get('/api/v1/customer/customer/:id', customer.getOne);
router.put('/api/v1/customer/customer/:id', customer.update);

//SHOP CATALOG
router.get('/api/v1/customer/products', product.getAll);

//MAKE ORDER
router.post('/api/v1/customer/order/', order.create);

router.get('/api/v1/customer/orders/all/:id', order.myOrders);



module.exports = router;