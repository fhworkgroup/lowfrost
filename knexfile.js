var config = require('./config/db_config.js');
module.exports = {
    development: config.development
    ,
    production: config.production
};
