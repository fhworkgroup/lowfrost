'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Finance = require('./finances');

var FixCost = bookshelf.Model.extend({
    tableName: 'fix_cost',

    finance: function () {
        return this.belongsTo(Finance);
    }

});

module.exports = bookshelf.model('FixCost', FixCost);