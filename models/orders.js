'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Customer = require('./customer');

/**
 * Customer Model mit Kardinalit�ten
 * @type {void|*|Object}
 */
var Orders = bookshelf.Model.extend({
    tableName: 'orders',
    idAttribute: 'order_id',

    customer: function() {
        return this.hasOne(Customer, 'customer_id');
    }

});

module.exports = bookshelf.model('Orders', Orders);