'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Order = require('./orders');

/**
 * Customer Model mit Kardinalit�ten
 * @type {void|*|Object}
 */
var Customer = bookshelf.Model.extend({
    tableName: 'customer',
    idAttribute: 'customer_id',

    orders: function () {
        return this.hasMany(Order, 'order_id');
    }
});

module.exports = bookshelf.model('Customer', Customer);