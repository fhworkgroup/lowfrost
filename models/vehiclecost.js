'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Finance = require('./finances');
var Vehicle = require('./vehicle');

var VehicleCost = bookshelf.Model.extend({
    tableName: 'vehicle_cost',
    idAttribute: 'vehicle_id',

    finance: function () {
        return this.belongsTo(Finance);
    },

    vehicle: function () {
        return this.belongsTo(Vehicle, 'vehicle_id');
    }

});

module.exports = bookshelf.model('VehicleCost', VehicleCost);