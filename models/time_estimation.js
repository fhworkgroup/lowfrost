'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Employer = require('./employer');

/**
 * Customer Model mit Kardinalit�ten
 * @type {void|*|Object}
 */
var TimeEstimation = bookshelf.Model.extend({
    tableName: 'time_estimation',
    idAttribute: 'date',

    employer: function () {
        return this.belongsTo(Employer, 'employer_id');
    }
});

module.exports = bookshelf.model('TimeEstimation', TimeEstimation);