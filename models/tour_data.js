'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var Bookshelf = require('./../bookshelf');
Bookshelf.plugin('registry');

var Vehicle = require('./vehicle');
var Tour = require('./tours');
var Employer = require('./employer');

var TourData = Bookshelf.Model.extend({
    tableName: 'tour_data',

    vehicle: function () {
        return this.hasOne(Vehicle, 'vehicle_id');
    },

    tour: function () {
        return this.belongsTo(Tour, 'tour_id');
    },

    employer: function () {
        return this.hasOne(Employer, 'employer_id');
    }

});

module.exports = Bookshelf.model('TourData', TourData);