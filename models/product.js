'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Orders = require('./orders');

/**
 * Customer Model mit Kardinalit�ten
 * @type {void|*|Object}
 */
var Product = bookshelf.Model.extend({
    tableName: 'product',
    idAttribute: 'product_id',

    basket: function() {
        return this.belongsToMany(Orders, 'order_id');
    }
});

module.exports = bookshelf.model('Product', Product);