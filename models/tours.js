'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var Bookshelf = require('./../bookshelf');
Bookshelf.plugin('registry');

var Vehicle = require('./vehicle');

var Tour = Bookshelf.Model.extend({
    tableName: 'tours',
    idAttribute: 'tour_id',

    vehicle: function () {
        return this.hasOne(Vehicle, 'vehicle_id');
    }
});

module.exports = Bookshelf.model('Tour', Tour);