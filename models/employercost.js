'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Finance = require('./finances');
var Employer = require('./employer');

var EmployerCost = bookshelf.Model.extend({
    tableName: 'employer_cost',
    idAttribute: 'employer_id',

    finance: function () {
        return this.belongsTo(Finance);
    },

    employer: function () {
        return this.belongsTo(Employer, 'employer_id');
    }

});

module.exports = bookshelf.model('EmployerCost', EmployerCost);