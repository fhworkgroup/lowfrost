'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */

var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Orders = require('./orders');
var Employer = require('./employer');
var Vehicle = require('./vehicle');

var Delivers = bookshelf.Model.extend({
    tableName: 'delivers',
    idAttribute: 'order_id',

    orders: function() {
        this.belongsToMany(Orders)
    },

    employer: function() {
        return this.belongsTo(Employer, 'employer_id');
    },

    vehicle: function () {
        return this.belongsTo(Vehicle, 'vehicle_id');
    }
});

module.exports = bookshelf.model('Delivers', Delivers);