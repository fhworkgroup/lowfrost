'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Finance = require('./finances');
var Product = require('./product');

var MaterialCost = bookshelf.Model.extend({
    tableName: 'material_cost',
    idAttribute: 'product_id',

    finance: function () {
        return this.belongsTo(Finance);
    },

    vehicle: function () {
        return this.belongsTo(Product, 'product_id');
    }

});

module.exports = bookshelf.model('MaterialCost', MaterialCost);