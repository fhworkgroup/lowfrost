'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Orders = require('./orders');
var Products = require('./product');

/**
 * Contains
 *
 * Relation
 */
var OrderItems = bookshelf.Model.extend({
    tableName: 'order_items',
    idAttribute: 'order_item_id',

    order: function() {
        return this.belongsToMany(Orders, 'order_id');
    },

    product: function() {
        return this.belongsToMany(Products, 'product_id');
    }
});

module.exports = bookshelf.model('OrderItems', OrderItems);