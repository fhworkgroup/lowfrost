'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Employer = require('./employer');

var Finance = bookshelf.Model.extend({
    tableName: 'finance',
    idAttribute: 'date'
});

module.exports = bookshelf.model('Finance', Finance);