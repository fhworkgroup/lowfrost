'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Tours = require('./tours');


var Vehicle = bookshelf.Model.extend({
    tableName: 'vehicle',
    idAttribute: 'vehicle_id',

    tours: function () {
        return this.hasMany(Tours, 'tour_id');
    }

});

module.exports = bookshelf.model('Vehicle', Vehicle);