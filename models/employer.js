'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');


var Orders = require('./orders');
var TimeEstimation = require('./time_estimation');

var Employer = bookshelf.Model.extend({
    tableName: 'employer',
    idAttribute: 'employer_id',

    orders: function () {
        return this.hasMany(Orders, 'order_id');
    },

    time_estimation: function() {
        return this.hasMany(TimeEstimation, 'date');
    }

});


module.exports = bookshelf.model('Employer', Employer);