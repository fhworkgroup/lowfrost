'use-strict'

/***
 * setup Bookshelf
 * @type {*|exports|module.exports}
 */
var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');

var Finance = require('./finances');
var Vehicle = require('./vehicle');

var FuelCost = bookshelf.Model.extend({
    tableName: 'fuel_cost',
    idAttribute: 'vehicle_id',

    finance: function () {
        return this.belongsTo(Finance);
    },

    vehicle: function () {
        return this.belongsTo(Vehicle, 'vehicle_id');
    }

});

module.exports = bookshelf.model('FuelCost', FuelCost);