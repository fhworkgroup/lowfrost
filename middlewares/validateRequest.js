var jwt = require('jwt-simple');
var validateUser = require('./../api/auth.js').validateUser;
var Basic = require('./../api/basic');

module.exports = function(req, res, next) {

    // When performing a cross domain request, you will recieve
    // a preflighted request first. This is to check if the app
    // is safe.

    // We skip the token outh for [OPTIONS] requests.
    //if(req.method == 'OPTIONS') next();


    var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
    var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];
    var role = (req.body && req.body.x_role) || (req.query && req.query.x_role) || req.headers['x-role'];

    if (token || key) {
        try {
            /**
             * Here we check if the jwt token from request is not expired
             *
             * @type {Object|{header, payload, signature}}
             */
            var decoded = jwt.decode(token, require('../config/secret.js')());
            if (decoded.exp <= Date.now()) {
                res.status(400);
                res.json({
                    "message": "Token Abgelaufen"
                });
                return;
            }

            /**
             * This will check if user exist
             * and if user has the right userRole to proceed
             *
             * returns not authorized if user has no acces rights,
             * or transmits request next
             */
            validateUser(role, key, function(user) {
                if (user) {
                    isAllowed(user, function() {
                        res.status(403);
                        res.json({
                            "message": "Nicht Autorisiert"
                        });
                    });
                } else {
                    // No user with this name exists, respond back with a 401
                    res.status(401);
                    res.json({
                        "message": "Ungueltiger Nutzer"
                    });
                    return;
                }
            }, function(err) {
                Basic.respondToClient(res, 404, "Fehler", "Benutzer nicht gefunden", err, null);
            }); // The key would be the logged in user's username

        } catch (err) {
            res.status(500);
            res.json({
                "message": "Oops da ging was schief :(",
            });
        }
    } else {
        res.status(401);
        res.json({
            "message": "Ungueltiger Token oder Schluessel"
        });
        return;
    }

    /**
     *This function compares the req.url & userRole from the request with
     * an array (allowedRoles) which contains our allowed user Roles strings.
     *
     * if the request url don�t contain one of our defined user roles, the request will
     * be denied and no access will be granted. Then a not authorized Error will be sent
     * to the client
     *
     * @param user - user object from request
     * @param successCb - callback fires if request url contains allowed user Roles
     * @param errCb - callback fires if request doesn�t contain one of allowed Roles
     */
    function isAllowed(user, errCb) {
        var allowedRoles = ['admin', 'owner', 'accounting', 'employer', 'customer'];
        var isAllowedRole = false;

        if(user) {
            for(var i = 0; i < allowedRoles.length; i++) {

                if((req.url.indexOf(allowedRoles[i]) >= 0 && user.role == allowedRoles[i]) &&
                    req.url.indexOf('/api/v1/') >= 0)
                {
                    //isAllowedRole = true;
                    next(); // once acces granted move on to next middleware
                    return;
                }

                if(!isAllowedRole && (i === allowedRoles.length - 1)) {
                    errCb();
                }
            }
        }

    }

};