'use-strict'

/***
 * This Module creates an instance of Bookshelf module,
 * which can be used to refer to it. It�s necessary to use
 * this module rather than to create a new bookshelf instance,
 * because it creates a connection pool for the current database!
 *
 * NOTE:
 * It�s not recommended to create more than one instances of bookshelf.
 * @type {*}
 */
var config = require('./config/db_config.js');
var knex = require('knex')(config.development);
module.exports = require('bookshelf')(knex);