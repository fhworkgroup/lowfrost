'use-strict'

var bookshelf = require('./../bookshelf');
bookshelf.plugin('registry');
var User = require('./../models/employer.js');

module.exports = bookshelf.Collection.extend({
    model: User
});