/***
 * DB Configuration
 * version 0.1.0
 */

module.exports = {
    development: {
        client: 'mysql',
        connection: {
            host: '127.0.0.1',
            database: 'test',
            user: 'root',
            password: '',
            charset: 'utf8',
            timezone: 'UTC'
        },
        pool: {
            min: 1,
            max: 1
        },
        debug: true,
        port: "3000"
    },
    production: {
        client: 'mysql',
        connection: {
            host: '127.0.0.1',
            database: 'quadro_fhwikiapi',
            user: 'quadro',
            password: 'run6223c3po4764mo',
            charset: 'utf8'
        },
        pool: {
            min: 1,
            max: 10
        },
        port: "64857"
    }
};